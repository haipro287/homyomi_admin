import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useIntl } from 'react-intl';
import { Form, Formik } from 'formik';
import { Link, useHistory } from 'react-router-dom';
import { Card, CardBody, CardHeader } from '../card';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import { ModifyForm } from '../common-types/common-type';
import _ from 'lodash';
import { ModifyEntityPage } from './modify-entity-page';
import { GetHomePage, InitValues } from "../helpers/common-function";
import { Spinner } from "react-bootstrap";
import { AxiosResponse } from "axios";
import { ArrowRight } from '@material-ui/icons';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function EntityCrudPage({
    entity,
    onModify,
    moduleName = 'COMMON_COMPONENT.CREATE_UPDATE.MODULE_NAME',
    code,
    username,
    get,
    formModel,
    actions,
    validation,
    loading,
    mode,
    setEditEntity,
    homePageUrl,
    allFormButton,
    onComments,
    showComment = true,
    optionHeaderField,
    submitDirect = true,
    submitSuccessMsg,
    formatter,
    backDetail,
    backHome,
    onSecondModify,
    canChange = true,
    isUser = false,
    setCanChangeStatus,
    showAlertDialog,
}: {
    // modifyModel: ModifyModel;
    moduleName?: string;
    entity?: any;
    onModify: (values: any) => Promise<any>;
    onSecondModify?: (values: any) => Promise<any>;
    code?: string;
    username?: string;
    get?: (code: string) => any;
    formModel: ModifyForm;
    mode?: 'horizontal' | 'vertical';
    actions?: any;
    validation?: any;
    autoFill?: any;
    loading?: boolean;
    setEditEntity?: (entity: any) => void;
    homePageUrl?: string
    allFormButton?: any;
    onComments?: (entity: any, data: { content: string }) => Promise<AxiosResponse<any>>;
    showComment?: boolean;
    optionHeaderField?: string;
    submitDirect?: boolean;
    submitSuccessMsg?: string;
    formatter?: (entity: any) => any;
    backDetail?: string;
    backHome?: boolean;
    canChange?: boolean;
    isUser?: boolean;
    setCanChangeStatus?: () => void;
    showAlertDialog?: () => void;
}) {
    const intl = useIntl();
    const history = useHistory();
    const initValues = useMemo(() => InitValues(formModel), [formModel]);
    const [disabledSubmit, setDisabledSubmit] = useState(false);
    // const [refresh, setRefresh] = useState<boolean>(false);

    const [entityForEdit, setEntityForEdit] = useState(entity);
    useEffect(() => {
        entity && setEntityForEdit({ ...entity, code: code });
    }, [entity])

    useEffect(() => {
        if (!code && !entity) setEntityForEdit({ initValues });
    }, [initValues, code]);
    const { _header, ...modifyPanels } = formModel;

    useEffect(() => {
        if (code) {
            get &&
                get(code).then((res: { data: any }) => {
                    const entityData = isUser ? {
                        ...res.data, code,
                        isBanned: {
                            label: res.data?.isBanned ?
                                intl.formatMessage({ id: 'USER.OPTION.BANNED' })
                                : intl.formatMessage({ id: 'USER.OPTION.ACTIVE' }),
                            value: res.data?.isBanned
                        }
                    } : { ...res.data, code }

                    setEntityForEdit(entityData);
                });
        }
    }, [code]);

    const notifyError = useCallback((error: string) => {
        const getError = (error: string): string | ({ message: string, additional: string }[]) => {
            try {
                return JSON.parse(error)
            } catch (e) {
                return error
            }
        };
        const _innerError = getError(error);

        toast.error(_.isString(_innerError) ? intl.formatMessage({ id: _innerError ?? 'COMMON_COMPONENT.TOAST.DEFAULT_ERROR' }, { additional: '' }) :
            (<span>{_innerError.map((e, index) => (
                (<span key={`abc${index}`} style={{ display: 'block' }}>{intl.formatMessage({ id: e.message }, e)}</span>)
            ))}</span>)
            , {
                position: 'top-right',
                autoClose: 3000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
    }, []);

    const notifySuccess = useCallback((message?: string) => {
        toast.success(intl.formatMessage({ id: message ?? 'COMMON_COMPONENT.TOAST.DEFAULT_SUCCESS' }), {
            position: 'top-right',
            autoClose: 2000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
    }, []);

    const handleSuccess = (values: any) => {
        submitSuccessMsg && notifySuccess(submitSuccessMsg);
        const idCodeUrl = backDetail ? values[backDetail] ? `/view/${values[backDetail]}` : code ? `/view/${code}` : '' : '';
        backDetail && history.push(`${homePageUrl ?? GetHomePage(window.location.pathname)}${idCodeUrl}`);
        // isUser && setRefresh(true)
        submitDirect && !backDetail &&
            history.push(homePageUrl ?? GetHomePage(window.location.pathname))
    }

    return (
        <>
            <ToastContainer />
            <Formik
                enableReinitialize={true}
                initialValues={entityForEdit}
                validationSchema={validation}
                onSubmit={(values, { setSubmitting, validateForm }) => {
                    let formatValues = formatter ? formatter(values) : values;

                    formatValues = isUser ? {
                        ...formatValues,
                        isBanned: formatValues?.isBanned?.value ?? formatValues?.isBanned?.label === intl.formatMessage({ id: 'USER.OPTION.ACTIVE' }) ? false : true
                    } : formatValues

                    if (canChange) {
                        onModify(formatValues).then((data) => {
                            if (!data.error) {
                                setCanChangeStatus && setCanChangeStatus();
                                if (onSecondModify) {
                                    onSecondModify(entityForEdit).then((secondData) => {
                                        if (!secondData.error) {
                                            handleSuccess(values)
                                        } else {
                                            console.log(secondData.error)
                                        }
                                    }).catch(err => {
                                        console.log(err)
                                    });
                                } else {
                                    handleSuccess(values)
                                }
                            }
                        }).catch((err) => {
                            // setSubmitting(false);
                        });
                    } else {
                        showAlertDialog && showAlertDialog();
                    }

                }}>
                {({ handleSubmit, setFieldValue, errors }) => (
                    <>
                        <Form className="form form-label-right">
                            {Object.keys(modifyPanels).map((key, index, keys) => {
                                const val = modifyPanels[key];
                                if (_.isString(val))
                                    throw new Error('Sử dụng sai cách ' + key + '\n' + JSON.stringify(modifyPanels));
                                const { _title, ...panel } = val;
                                console.log({ errors })
                                return (
                                    <Card key={`entity-crud-card` + key} className={'modify-page'}>
                                        <CardHeader
                                            className={'border-bottom-0'}
                                            title={
                                                index == 0 ? (
                                                    // eslint-disable-next-line jsx-a11y/anchor-is-valid
                                                    <a
                                                        onClick={backHome ? (() => history.push(homePageUrl ?? GetHomePage(window.location.pathname))) : (() => history.goBack())}
                                                        className={'cursor-pointer text-primary font-weight-boldest'}>
                                                        <ArrowBackIosIcon />
                                                        {_.isString(_header) ? intl
                                                            .formatMessage(
                                                                { id: _header },
                                                                { moduleName: intl.formatMessage({ id: moduleName ?? 'EMPTY' }) },
                                                            )
                                                            .toUpperCase() : _header(entityForEdit)}
                                                        {(optionHeaderField && entityForEdit) ? <>
                                                            <ArrowRight style={{ "verticalAlign": "text-top" }} />
                                                            {entityForEdit[optionHeaderField] ?? ""}
                                                        </> : ""}
                                                    </a>
                                                ) : (
                                                    <>
                                                        {intl
                                                            .formatMessage(
                                                                { id: _title },
                                                                { moduleName: intl.formatMessage({ id: moduleName ?? 'EMPTY' }) },
                                                            )
                                                            .toUpperCase()}
                                                    </>
                                                )
                                            }
                                        />
                                        <CardBody>
                                            <ModifyEntityPage
                                                mode={mode}
                                                // className={formPart[key].className}
                                                // images={images}
                                                inputGroups={panel}
                                                errors={errors}
                                            />
                                            {allFormButton && allFormButton.type === 'outside' && (
                                                <div className="text-right mt-10">
                                                    {Object.keys(allFormButton.data).map(keyss => {
                                                        switch (allFormButton['data'][keyss].role) {
                                                            case 'submit':
                                                                return (
                                                                    <button
                                                                        type={allFormButton['data'][keyss].type}
                                                                        onClick={() => {
                                                                            allFormButton['data'][keyss].onClick();
                                                                        }}
                                                                        className={allFormButton['data'][keyss].className}
                                                                        key={keyss}>
                                                                        {allFormButton['data'][keyss].icon} {allFormButton['data'][keyss].label}
                                                                    </button>
                                                                );

                                                            case 'special':
                                                                return (
                                                                    <button
                                                                        type={allFormButton['data'][keyss].type}
                                                                        onClick={() => {
                                                                            // handleSubmit();
                                                                            allFormButton['data'][keyss].onClick(entityForEdit);
                                                                        }}
                                                                        className={allFormButton['data'][keyss].className}
                                                                        key={keyss}>
                                                                        {allFormButton['data'][keyss].icon} {allFormButton['data'][keyss].label}
                                                                    </button>
                                                                );
                                                            case 'button':
                                                                return (
                                                                    <button
                                                                        type={allFormButton['data'][keyss].type}
                                                                        className={allFormButton['data'][keyss].className}
                                                                        key={keyss}
                                                                        onClick={() => {
                                                                            allFormButton['data'][keyss].onClick(entityForEdit);
                                                                        }}>
                                                                        {allFormButton['data'][keyss].icon} {allFormButton['data'][keyss].label}
                                                                    </button>
                                                                );
                                                            case 'link-button':
                                                                return (
                                                                    <Link to={allFormButton['data'][keyss].linkto}
                                                                        key={keyss}>
                                                                        <button
                                                                            type={allFormButton['data'][keyss].type}
                                                                            className={allFormButton['data'][keyss].className}>
                                                                            {allFormButton['data'][keyss].icon} {allFormButton['data'][keyss].label}
                                                                        </button>
                                                                    </Link>
                                                                );
                                                        }
                                                    })}
                                                </div>
                                            )}
                                            {(actions && index === Object.keys(modifyPanels).length - 1) && (
                                                <div className="text-right mt-10" key={key}>
                                                    {Object.keys(actions.data).map(keyss => {
                                                        switch (actions.data[keyss].role) {
                                                            case 'submit':
                                                                return (
                                                                    <button
                                                                        id={actions.data[keyss].id ?? 'btn-submit'}
                                                                        className={actions.data[keyss].className}
                                                                        key={keyss}
                                                                        type={'submit'}
                                                                        onClick={() => {
                                                                            if (Object.keys(errors).length > 0) {

                                                                                notifyError('COMMON.NEED_FILL_ALL_REQUIRED_FIELDS');
                                                                            }
                                                                        }}
                                                                    >
                                                                        {loading === true ? actions.data[keyss].loading ?? (
                                                                            <Spinner animation="border" variant="light"
                                                                                size="sm" />) : actions.data[keyss].icon} {intl.formatMessage({ id: actions.data[keyss].label })}
                                                                    </button>
                                                                );

                                                            case 'button':
                                                                return (
                                                                    <button
                                                                        type={actions.data[keyss].type}
                                                                        className={actions.data[keyss].className}
                                                                        key={keyss}>
                                                                        {actions.data[keyss].icon} {intl.formatMessage({ id: actions.data[keyss].label })}
                                                                    </button>
                                                                );

                                                            case 'link-button':
                                                                const paramOption = actions?.data[keyss]?.optionParam;
                                                                const paramField = actions?.data[keyss]?.optionField;
                                                                const guard = actions?.data[keyss]?.guard;
                                                                if (!guard) {
                                                                    return (<></>)
                                                                }
                                                                const paramUrl = paramOption && entityForEdit && entityForEdit[paramField] ? `?${paramOption}=${entityForEdit[paramField]}` : '';
                                                                const queryID = actions?.data[keyss]?.queryID;
                                                                var link = actions.data[keyss].linkto + (queryID ? `/${code}` : "");
                                                                link = link + paramUrl;

                                                                return (
                                                                    <Link to={link} key={keyss}>
                                                                        <button
                                                                            type={actions.data[keyss].type}
                                                                            className={actions.data[keyss].className}>
                                                                            {actions.data[keyss].icon} {intl.formatMessage({ id: actions.data[keyss].label })}
                                                                        </button>
                                                                    </Link>
                                                                );
                                                        }
                                                    })}
                                                </div>)
                                            }
                                        </CardBody>
                                    </Card>
                                );
                            })}

                        </Form>
                        {/* {showComment !== true || !onComments ? <></> : (entityForEdit?.confirmationStatus === '3'
                  || (entityForEdit?.isMaster === true && entityForEdit?.confirmationStatus === '2')) ? (
                  <Card>
                      <CardBody className={'p-0'}>
                          <div className="mb-5 mt-5">
                              <span className="text-primary detail-dialog-subtitle mt-8">BÌNH LUẬN</span>
                              <div className="mt-8 border border-light rounded pt-5 pb-5">
                                  {entityForEdit && entityForEdit.comments.length > 0 ? (
                                      entityForEdit.comments.map(
                                          (
                                              value: { createdBy: { _id: string; fullName: string }; content: string },
                                              key: number,
                                          ) => (
                                              <div key={key} className="row mb-3">
                                                  <div className="col-1 text-center">
                                                      <AccountCircleOutlinedIcon style={{ fontSize: 30 }} />
                                                  </div>
                                                  <div className="col-10 bg-light rounded p-3">
                                                      <p className="font-bold">{value.createdBy.fullName}</p>
                                                      <p>{value.content}</p>
                                                  </div>
                                              </div>
                                          ),
                                      )
                                  ) : (
                                      <span>Chưa có bình luận</span>
                                  )}
                              </div>
                          </div>
                      </CardBody>
                  </Card>
              ) : (
                  <PostComments entity={entityForEdit} onComments={onComments} />
              )} */}
                    </>
                )}
            </Formik>
        </>
    );
}

export default EntityCrudPage;
