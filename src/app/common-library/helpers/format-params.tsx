import { isString } from "lodash";


const formatUpperLowerToNormal = (string: any) => {
    if (!isString(string)) return string;
    return string.match(/[A-Z]*[^A-Z]+/g)?.join('_').toLowerCase() ?? "";
}

const formatObjectKeyToLodash = (object: any) => {
    const keyValues = Object.keys(object).map(key => {
        let newKey = formatUpperLowerToNormal(key);
        if (['from_user', 'to_user'].includes(newKey) && /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(object[key])) {
            newKey = newKey.replace('user', 'mail');
        }
        return { [newKey]: object[key] };
    });
    return Object.assign({}, ...keyValues);
}

export const formatTimeToUnix = (time: any) => {
    if (time.toString().match(/^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9]).([0-9][0-9][0-9]+)?(Z)?$/g)) {
        const unix = new Date(time).getTime()
        // const unixRound = Math.round(unix);
        return unix;
    } else {
        return time;
    }
}

const formatObjectValueToLodash = (object: any) => {
    const keyValues = Object.keys(object).map((key) => {
        let newValue = formatTimeToUnix(object[key]);
        newValue = formatUpperLowerToNormal(newValue);
        return { [key]: newValue };
    });
    return Object.assign({}, ...keyValues);
}

export const formatParamsGet = (queryProps: any, sortList: any, paginationProps: any, skipCheckValue?: boolean) => {
    const newQueryProps = formatObjectKeyToLodash(queryProps);
    const newPaginationProps = !skipCheckValue ? formatObjectValueToLodash(paginationProps) : paginationProps;

    // let formatQueryProps = { ...newQueryProps };
    let formatQueryProps = { ...newQueryProps };
    console.log(formatQueryProps);

    Object.keys(formatQueryProps).forEach(key => {
        if (formatQueryProps[key] == null || formatQueryProps[key] === undefined || formatQueryProps[key] == "") {
            delete formatQueryProps[key];
            return
        }
        formatQueryProps[`filter%5B${key}%5D`] = formatQueryProps[key];
        delete formatQueryProps[key];
    })

    if (newQueryProps.is_banned != null && newQueryProps.is_banned !== "" && formatQueryProps !== undefined) {
        formatQueryProps = { ...formatQueryProps, is_banned: newQueryProps.is_banned }
    }
    if (newQueryProps.role != null && newQueryProps.role !== "" && formatQueryProps !== undefined) {
        formatQueryProps = { ...formatQueryProps, role: newQueryProps.role }
    }

    console.log(newPaginationProps);

    const limit = newPaginationProps?.limit || 5;

    const offset = ((newPaginationProps?.page ?? 1) - 1) * (newPaginationProps?.limit ?? 5);

    const sortBy = newPaginationProps?.sortBy;

    const sortType = newPaginationProps?.sortType?.toString().toUpperCase() || 'ASC';

    let params = sortBy ? { ...formatQueryProps, page: newPaginationProps?.page, perPage: limit, sortBy, sortType }
        : { ...formatQueryProps, page: newPaginationProps?.page, perPage: limit }

    // console.log({ params: formatObjectValueToLodash(params) })
    return !skipCheckValue ? formatObjectValueToLodash(params) : params;
}
