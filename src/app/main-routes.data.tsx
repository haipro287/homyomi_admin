import { MenuItemModel } from './layout/components/aside/aside-menu/menu-item-model';
import React, { lazy } from 'react';
import SVG from "react-inlinesvg";
import { ToAbsoluteUrl } from "./common-library/helpers/assets-helpers";

const HomePage = lazy(() => import('./pages/_homepage'));
const UserPage = lazy(() => import('./pages/user/user'));
const ActivityPage = lazy(() => import('./pages/activity/activity'));
const AuthorPage = lazy(() => import('./pages/author/author'));
const CategoryPage = lazy(() => import('./pages/category/category'));
const AdminPage = lazy(() => import('./pages/admin/admin'));
const MangaPage = lazy(() => import('./pages/manga/manga'));
const RecommendedPage = lazy(() => import('./pages/recommend/recommend'));

export const MainRoutes: MenuItemModel[] = [
  // {parent: true, title: 'MENU.DASHBOARD', url: '/dashboard', component: HomePage},
  { parent: true, title: 'MENU.USER_MANAGEMENT', url: '/user-management', component: UserPage },
  { parent: true, title: 'MENU.AUTHOR_MANAGEMENT', url: '/author-management', component: AuthorPage },
  { parent: true, title: 'MENU.CATEGORY_MANAGEMENT', url: '/category-management', component: CategoryPage },
  {
    parent: true, title: 'MENU.MANGA_MANAGEMENT', url: '/manga-management',
    // children: [
    //   {
    //     title: 'MENU.USER.ROLE',
    //     url: 'role',
    //     icon: 'role.svg',
    //     component: MangaPage
    //   },
    //   {
    //     title: 'MENU.USER.ACCOUNT',
    //     url: 'user',
    //     icon: (
    //       <SVG src={ToAbsoluteUrl('/media/svg/vncheck/account.svg')} style={{ width: '17px' }}/>
    //     ),
    //     component: UserPage
    //   },
    // ],
    component: MangaPage
  },
  { parent: true, title: 'MENU.RECOMMENDED', url: '/recommended', component: RecommendedPage },
  // { parent: true, title: 'MENU.ACTIVITY_LOG', url: '/activity', component: ActivityPage },
  // { parent: true, title: 'MENU.ADMIN_MANAGEMENT', url: '/admin-management', component: AdminPage },
];
