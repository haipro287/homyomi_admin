import React, {useMemo} from 'react';
import objectPath from 'object-path';
import {SearchDropdown} from "../extras/dropdowns/search/search-dropdown";
import {useHtmlClassService} from "../../_core/metronic-layout";
import {UserNotificationsDropdown} from "../extras/dropdowns/user-notifications-dropdown";
import {QuickUserToggler} from "../extras/quick-user-toggler";

export function Topbar() {
  const uiService: any = useHtmlClassService();
  
  const layoutProps = useMemo(() => {
    return {
      viewSearchDisplay: objectPath.get(uiService.config, 'extras.search.display'),
      viewNotificationsDisplay: objectPath.get(uiService.config, 'extras.notifications.display'),
      viewQuickActionsDisplay: objectPath.get(uiService.config, 'extras.quick-actions.display'),
      viewCartDisplay: objectPath.get(uiService.config, 'extras.cart.display'),
      viewQuickPanelDisplay: objectPath.get(uiService.config, 'extras.quick-panel.display'),
      viewLanguagesDisplay: objectPath.get(uiService.config, 'extras.languages.display'),
      viewUserDisplay: objectPath.get(uiService.config, 'extras.user.display'),
    };
  }, [uiService]);
  
  return (
    <div className="topbar">
      {layoutProps.viewNotificationsDisplay && <UserNotificationsDropdown/>}
      {layoutProps.viewUserDisplay && <QuickUserToggler/>}
    </div>
  );
}
