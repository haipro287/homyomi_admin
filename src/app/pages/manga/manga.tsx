import React, { Fragment, useCallback, useEffect, useMemo, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import {
  DefaultPagination,
  HomePageURL,
  iconStyle,
  NormalColumn,
  SortColumn
} from '../../common-library/common-consts/const';
import { MasterHeader } from '../../common-library/common-components/master-header';
import {
  MasterBodyColumns,
  ModifyForm,
  ModifyInputGroup, ModifyPanel,
  RenderInfoDetail,
  SearchModel
} from '../../common-library/common-types/common-type';
import { InitMasterProps, } from '../../common-library/helpers/common-function';
import { Route, Switch, useHistory } from 'react-router-dom';
import { Count, Create, Delete, DeleteMany, Get, GetAll, GetById, Update } from "./manga.service";
import { MasterBody } from "../../common-library/common-components/master-body";
import { ActionsColumnFormatter } from '../../common-library/common-components/actions-column-formatter';
import EntityCrudPage from '../../common-library/common-components/entity-crud-page';
import { Spinner } from 'react-bootstrap';
import SaveOutlinedIcon from '@material-ui/icons/SaveOutlined';
import CancelOutlinedIcon from '@material-ui/icons/CancelOutlined';
import * as ProviderService from "../author/author.service";
import { NotifyDialog } from '../../common-library/common-components/notify-dialog';
import { RootStateOrAny, useSelector } from 'react-redux';
import { DisplayDateTime, DisplayPercent, DisplayTable } from '../../common-library/helpers/detail-helpers';
import { Edit } from '@material-ui/icons';
import { Tooltip } from '@material-ui/core';
import { DeleteEntityDialog } from "../../common-library/common-components/delete-entity-dialog";
import { useField } from "formik";
import * as Author from "../author/author.service";
import * as Category from "../category/category.service";
import * as Chapter from "./chapter.service";
import AddIcon from "@material-ui/icons/Add";
import axios from "axios";
import * as MangaService from "./manga.service";

const headerTitle = 'MANGA.MASTER.HEADER.TITLE';

const DisplayTb = (props: any): any => {
  const [field, meta, helpers] = useField<any>(props.name);
  console.log(field.value);

  return <DisplayTable entities={field.value ?? []} columns={props.columns}/>;
};

function Manga() {
  const intl = useIntl();

  const {
    entities,
    setEntities,
    deleteEntity,
    setDeleteEntity,
    createEntity,
    setCreateEntity,
    selectedEntities,
    editEntity,
    setEditEntity,
    setSelectedEntities,
    detailEntity,
    showDelete,
    setShowDelete,
    showDeleteMany,
    setShowDeleteMany,
    showDetail,
    setShowDetail,
    paginationProps,
    setPaginationProps,
    filterProps,
    setFilterProps,
    total,
    loading,
    setLoading,
    error,
    add,
    update,
    get,
    convertUnixTimeToDate,
    deleteMany,
    deleteFn,
    getAll,
    formatLongString,
  } = InitMasterProps<any>({
    getServer: Get,
    countServer: Count,
    createServer: Create,
    deleteServer: Delete,
    deleteManyServer: DeleteMany,
    getAllServer: GetAll,
    updateServer: Update,
  });

  const createTitle = 'MANGA.MASTER.TABLE.CREATE';
  const updateTitle = 'MANGA.MASTER.TABLE.UPDATE';
  const viewTitle = 'MANGA.MASTER.TABLE.TITLE';

  const [currentTab, setCurrentTab] = useState<string | undefined>('0');
  const [trigger, setTrigger] = useState<boolean>(false);
  const [showPermissionDeny, setShowPermissionDeny] = useState<boolean>(false);

  const userInfo = useSelector(({ auth }: { auth: RootStateOrAny }) => auth);

  const history = useHistory();

  useEffect(() => {
    getAll(filterProps);
  }, [paginationProps, filterProps, trigger, currentTab]);

  const columns = useMemo(() => {
    return [
      {
        dataField: 'id',
        text: `${intl.formatMessage({ id: 'MANGA.MASTER.TABLE.ID' })}`,
        headerClasses: 'text-center',
        align: 'center',
      },
      // {
      //   dataField: 'id',
      //   text: `${intl.formatMessage({ id: 'MANGA.MASTER.TABLE.CODE' })}`,
      //   headerClasses: 'text-center',
      //   formatter: (input: any) => <Tooltip children={<span>{formatLongString(input, 10, 10)}</span>} title={input} />,
      //   align: 'center',
      // },
      {
        dataField: 'title',
        text: `${intl.formatMessage({ id: 'MANGA.MASTER.TABLE.NAME' })}`,
        headerClasses: 'text-center',
        formatter: (input: any) => <Tooltip children={<span>{formatLongString(input, 10, 10)}</span>} title={input}/>,
        align: 'center',
      },
      {
        dataField: 'romajiTitle',
        text: `${intl.formatMessage({ id: 'MANGA.MASTER.TABLE.NAME' })}`,
        headerClasses: 'text-center',
        formatter: (input: any) => <Tooltip children={<span>{formatLongString(input, 10, 10)}</span>} title={input}/>,
        align: 'center',
      },
      {
        dataField: 'synopsis',
        text: `${intl.formatMessage({ id: 'MANGA.MASTER.TABLE.NAME' })}`,
        headerClasses: 'text-center',
        formatter: (input: any) => <Tooltip children={<span>{formatLongString(input, 10, 10)}</span>} title={input}/>,
        align: 'center',
      },
      {
        dataField: 'authors',
        text: `${intl.formatMessage({ id: 'PROVIDER.MASTER.TABLE.PROVIDER_NAME' })}`,
        headerClasses: 'text-center',
        formatter: (input: any[]) => <Tooltip children={<span>{formatLongString(input[0] ?? "", 10, 10)}</span>}
                                              title={input.reduce((a, b, c) => {
                                                return `${a} ${b}`;
                                              }, "")}/>,
        align: 'center',
      },
      {
        dataField: 'categories',
        text: `${intl.formatMessage({ id: 'PROVIDER.MASTER.TABLE.PROVIDER_NAME' })}`,
        headerClasses: 'text-center',
        formatter: (input: any[]) => <Tooltip children={<span>{formatLongString(input[0] ?? "", 10, 10)}</span>}
                                              title={input.reduce((a, b, c) => {
                                                return `${a} ${b}`;
                                              }, "")}/>,
        align: 'center',
      },
      // {
      //   dataField: 'providerId',
      //   text: `${intl.formatMessage({ id: 'PROVIDER.MASTER.TABLE.PROVIDER_ID' })}`,
      //   align: 'center',
      //   formatter: (input: any) => <Tooltip children={<span>{formatLongString(input, 10, 10)}</span>} title={input} />,
      //   headerClasses: 'text-center',
      // },
      {
        dataField: 'mangaType',
        class: "btn-primary",
        text: `${intl.formatMessage({ id: 'MANGA.MASTER.TABLE.TYPE' })}`,
        headerClasses: 'text-center',
        classes: 'text-center',
      },
      {
        dataField: 'ageRating',
        class: "btn-primary",
        text: `${intl.formatMessage({ id: 'MANGA.MASTER.TABLE.RATING' })}`,
        headerClasses: 'text-center',
        classes: 'text-center',
      },
      {
        dataField: 'status',
        class: "btn-primary",
        text: `${intl.formatMessage({ id: 'MANGA.MASTER.TABLE.STATUS' })}`,
        headerClasses: 'text-center',
        classes: 'text-center',
      },
      {
        dataField: 'createdAt',
        text: `${intl.formatMessage({ id: 'MANGA.MASTER.TABLE.JOIN_DATE' })}`,
        // formatter: (input: any) => <FormattedDate value={convertUnixTimeToDate(input)} />,
        headerClasses: 'text-center',
        align: 'center',
      },
      // {
      //   dataField: 'updatedAt',
      //   text: `${intl.formatMessage({ id: 'MANGA.MASTER.TABLE.JOIN_DATE' })}`,
      //   // formatter: (input: any) => <FormattedDate value={convertUnixTimeToDate(input)} />,
      //   headerClasses: 'text-center',
      //   align: 'center',
      // },
      // {
      //   dataField: 'type',
      //   text: `${intl.formatMessage({ id: 'MANGA.MASTER.TABLE.TYPE' })}`,
      //   align: 'center',
      //   formatter: (input: any) => <FormattedMessage id={`MANGA.MANGA.OPTION_${input.toString().toUpperCase()}`} />,
      //   headerClasses: 'text-center',
      // },
      {
        dataField: 'action',
        text: `${intl.formatMessage({ id: 'MANGA.MASTER.TABLE.ACTION_COLUMN' })}`,
        formatter: ActionsColumnFormatter,
        formatExtraData: {
          intl,
          onShowDetail: (entity: any) => {
            history.push(`${window.location.pathname}/view/${entity.id}`);
          },
          // onEdit: (entity: any) => {
          //   // if (["service-admin", "super-admin"].includes(userInfo.role)) {
          //   if (true) {
          //     setEditEntity(entity);
          //     history.push(`${window.location.pathname}/${entity.id}`);
          //   } else {
          //     setShowPermissionDeny(true);
          //   }
          // },
          // onDelete: (entity: any) => {
          //   // if (["service-admin", "super-admin"].includes(userInfo.role)) {
          //   if(true) {
          //     setDeleteEntity(entity);
          //     setShowDelete(true);
          //   } else {
          //     setShowPermissionDeny(true)
          //   }
          // },
        },
        ...NormalColumn,
        style: { minWidth: '130px' },
      }
    ];
  }, []);

  const searchModel: SearchModel = useMemo(() => ({
    serviceId: {
      type: 'string',
      label: 'MANGA.MASTER.SEARCH.CODE',
      placeholder: 'MANGA.MASTER.SEARCH.CODE'
    },
    name: {
      type: 'string',
      label: 'MANGA.MASTER.SEARCH.NAME',
      placeholder: 'MANGA.MASTER.SEARCH.NAME'
    },
  }), [currentTab]);

  const [group1, setGroup1] = useState<ModifyInputGroup>({
    _subTitle: '',
    // serviceId: {
    //   _type: 'string-alpha',
    //   label: 'MANGA.MASTER.TABLE.MANGA_ID',
    //   required: true,
    // },
    canonicalTitle: {
      _type: 'string',
      label: 'MANGA.MASTER.TABLE.CANONICAL_TITLE',
      required: true,
    },
    romajiTitle: {
      _type: 'string',
      label: 'MANGA.MASTER.TABLE.ROMAJI_TITLE',
      required: true,
    },
    titleEn: {
      _type: 'string',
      label: 'MANGA.MASTER.TABLE.TITLE_EN',
      required: false,
    },
    titleJa: {
      _type: 'string',
      label: 'MANGA.MASTER.TABLE.TITLE_JA',
      required: false,
    },
    titleVi: {
      _type: 'string',
      label: 'MANGA.MASTER.TABLE.TITLE_VI',
      required: false,
    },
    authors: {
      _type: 'search-select',
      label: 'MANGA.MASTER.TABLE.AUTHORS',
      onSearch: Author.GetAll,
      keyField: "name",
      hasMore: true,
      onChange: (input: any) => input.id,
    },
    categories: {
      _type: 'search-select',
      label: 'MANGA.MASTER.TABLE.CATEGORIES',
      onSearch: Category.GetAll,
      keyField: "title",
      hasMore: true,
      onChange: (input: any) => input.id,
    },
    description: {
      _type: 'text-field',
      label: 'MANGA.MASTER.TABLE.SYNOPSIS',
      required: true,
    },
    mangaType: {
      _type: 'radio',
      label: 'MANGA.MASTER.TABLE.TYPE',
      options: [
        { value: "doujin", label: "MANGA.TYPE.DOUJIN" },
        { value: "manga", label: "MANGA.TYPE.MANGA" },
        { value: "manhua", label: "MANGA.TYPE.MANHUA" },
        { value: "manhwa", label: "MANGA.TYPE.MANHWA" },
        { value: "novel", label: "MANGA.TYPE.NOVEL" },
        { value: "oel", label: "MANGA.TYPE.OEL" },
        { value: "oneshot", label: "MANGA.TYPE.ONESHOT" },
      ],
      required: true,
    },
    ageRating: {
      _type: 'radio',
      label: 'MANGA.MASTER.TABLE.RATING',
      options: [
        { value: "G", label: "MANGA.RATING.G" },
        { value: "PG", label: "MANGA.RATING.PG" },
        { value: "R", label: "MANGA.RATING.R" },
        { value: "R18", label: "MANGA.MANGA.R18" },
      ],
      required: true,
    },
    status: {
      _type: 'radio',
      label: 'MANGA.MASTER.TABLE.STATUS',
      options: [
        { value: "ongoing", label: "MANGA.STATUS.ONGOING" },
        { value: "finished", label: "MANGA.STATUS.FINISHED" },
        { value: "dropped", label: "MANGA.STATUS.DROPPED" },
      ],
      required: true,
    },

  });

  const [editGroup1, setEditGroup1] = useState<ModifyInputGroup>({
    _subTitle: '',
    canonicalTitle: {
      _type: 'string',
      label: 'MANGA.MASTER.TABLE.CANONICAL_TITLE',
      required: true,
    },
    romajiTitle: {
      _type: 'string',
      label: 'MANGA.MASTER.TABLE.ROMAJI_TITLE',
      required: true,
    },
    titleEn: {
      _type: 'string',
      label: 'MANGA.MASTER.TABLE.TITLE_EN',
      required: false,
    },
    titleJa: {
      _type: 'string',
      label: 'MANGA.MASTER.TABLE.TITLE_JA',
      required: false,
    },
    titleVi: {
      _type: 'string',
      label: 'MANGA.MASTER.TABLE.TITLE_VI',
      required: false,
    },
    authors: {
      _type: 'search-select',
      label: 'MANGA.MASTER.TABLE.AUTHORS',
      onSearch: Author.GetAll,
      keyField: "name",
      hasMore: true,
      onChange: (input: any) => input.id,
    },
    categories: {
      _type: 'search-select',
      label: 'MANGA.MASTER.TABLE.CATEGORIES',
      onSearch: Category.GetAll,
      keyField: "title",
      hasMore: true,
      onChange: (input: any) => input.id,
    },
    description: {
      _type: 'text-field',
      label: 'MANGA.MASTER.TABLE.SYNOPSIS',
    },
    mangaType: {
      _type: 'radio',
      label: 'MANGA.MASTER.TABLE.TYPE',
      options: [
        { value: "doujin", label: "MANGA.TYPE.DOUJIN" },
        { value: "manga", label: "MANGA.TYPE.MANGA" },
        { value: "manhua", label: "MANGA.TYPE.MANHUA" },
        { value: "manhwa", label: "MANGA.TYPE.MANHWA" },
        { value: "novel", label: "MANGA.TYPE.NOVEL" },
        { value: "oel", label: "MANGA.TYPE.OEL" },
        { value: "oneshot", label: "MANGA.TYPE.ONESHOT" },
      ],
      required: true,
    },
    ageRating: {
      _type: 'radio',
      label: 'MANGA.MASTER.TABLE.RATING',
      options: [
        { value: "G", label: "MANGA.RATING.G" },
        { value: "PG", label: "MANGA.RATING.PG" },
        { value: "R", label: "MANGA.RATING.R" },
        { value: "R18", label: "MANGA.MANGA.R18" },
      ],
      required: true,
    },
    status: {
      _type: 'radio',
      label: 'MANGA.MASTER.TABLE.STATUS',
      options: [
        { value: "ongoing", label: "MANGA.STATUS.ONGOING" },
        { value: "finished", label: "MANGA.STATUS.FINISHED" },
        { value: "dropped", label: "MANGA.STATUS.DROPPED" },
      ],
      required: true,
    },
  });

  const [viewGroup1, setViewGroup1] = useState<ModifyInputGroup>({
    _className: 'col-8 pl-xl-15 pl-md-10 pl-5',
    _subTitle: '',
    id: {
      _type: 'string',
      label: 'MANGA.MASTER.TABLE.MANGA_ID',
      disabled: true,
    },
    title: {
      _type: 'string',
      label: 'MANGA.MASTER.TABLE.NAME',
      disabled: true,
    },
    romajiTitle: {
      _type: 'string',
      label: 'MANGA.MASTER.TABLE.ROMAJI_NAME',
      disabled: true,
    },
    // providerId: {
    //   _type: 'search-select',
    //   required: true,
    //   label: 'PROVIDER.MASTER.TABLE.PROVIDER_ID',
    //   onSearch: ProviderService.GetAll,
    //   keyField: "providerId",
    //   hasMore: true,
    //   disabled: true,
    // },
    mangaType: {
      _type: 'string',
      disabled: true,
      label: 'MANGA.MASTER.TABLE.TYPE',
      // formatter: (input) => {
      //   return input ? intl.formatMessage({ id: `MANGA.MANGA.OPTION_${input}` }) : '';
      // }
    },
    ageRating: {
      _type: 'string',
      disabled: true,
      label: 'MANGA.MASTER.TABLE.RATING',
      // formatter: (input) => {
      //   return input ? intl.formatMessage({ id: `MANGA.MANGA.OPTION_${input}` }) : '';
      // }
    },
    synopsis: {
      _type: 'text-field',
      label: 'MANGA.MASTER.TABLE.NOTE',
      disabled: true,
    },
    // publicKey: {
    //   _type: 'string',
    //   required: true,
    //   label: 'MANGA.MASTER.TABLE.PUBLIC_KEY',
    //   disabled: true,
    // },
    // viewUrl: {
    //   _type: 'string',
    //   required: true,
    //   label: 'MANGA.MASTER.TABLE.URL_VIEW',
    //   disabled: true,
    // },
    // url: {
    //   required: true,
    //   _type: 'string',
    //   label: 'MANGA.MASTER.TABLE.URL_LOGIN',
    //   disabled: true,
    // },
  });

  const [viewGroup2, setViewGroup2] = useState<ModifyInputGroup>({
    _className: 'col-4 pl-xl-15 pl-md-10 pl-5',
    _subTitle: '',

    createdAt: {
      _type: 'date-time',
      label: 'MANGA.MASTER.TABLE.JOIN_DATE',
      disabled: true,
    },

    icon: {
      _type: 'image',
      label: 'MANGA.MASTER.TABLE.IMAGE',
      maxNumber: 1,
      isArray: false,
      disabled: true,
      width: "100%",
      height: "auto",
    },

  });

  const viewActions: any = useMemo(() => ({
    type: 'inside',
    data: {
      edit: {
        role: 'link-button',
        type: 'link',
        linkto: `/service-management`,
        queryID: true,
        className: 'ml-8 btn btn-primary fixed-btn-width',
        label: 'COMMON.BTN_EDIT_USER',
        icon: <Edit/>,
        guard: ["service-admin", "super-admin"].includes(userInfo.role),
      },
    }
  }), [loading]);

  const chapterModel = useMemo((): MasterBodyColumns => ({
    id: {
      dataField: 'id',
      text: `${intl.formatMessage({ id: 'MANGA.MASTER.CHAPTER.ID' })}`,
      ...SortColumn,
    },
    title: {
      dataField: 'title',
      text: `${intl.formatMessage({ id: 'MANGA.MASTER.CHAPTER.TITLE' })}`,
      ...SortColumn,
    },
    number: {
      dataField: 'number',
      text: `${intl.formatMessage({ id: 'MANGA.MASTER.CHAPTER.NUMBER' })}`,
      // formatter: DisplayPercent,
      ...SortColumn,
    },
    translatedLanguage: {
      dataField: 'translatedLanguage',
      text: `${intl.formatMessage({ id: 'MANGA.MASTER.CHAPTER.LANGUAGE' })}`,
      ...SortColumn,
    },
    action: {
      dataField: 'action',
      text: `${intl.formatMessage({ id: 'MANGA.MASTER.CHAPTER.ACTION_COLUMN' })}`,
      formatter: ActionsColumnFormatter,
      formatExtraData: {
        intl,
        onShowDetail: (entity: any) => {
          history.push(`${window.location.pathname}/view/${entity.id}`);
        },
        onEdit: (entity: any) => {
          // if (["service-admin", "super-admin"].includes(userInfo.role)) {
          if (true) {
            setEditEntity(entity);
            history.push(`${window.location.pathname}/${entity.id}`);
          } else {
            setShowPermissionDeny(true);
          }
        },
        // onDelete: (entity: any) => {
        //   // if (["service-admin", "super-admin"].includes(userInfo.role)) {
        //   if(true) {
        //     setDeleteEntity(entity);
        //     setShowDelete(true);
        //   } else {
        //     setShowPermissionDeny(true)
        //   }
        // },
      },

    }
  }), []);

  const authorModel = useMemo((): MasterBodyColumns => ({
    id: {
      dataField: 'id',
      text: `${intl.formatMessage({ id: 'MANGA.MASTER.AUTHOR.ID' })}`,
      ...SortColumn,
    },
    name: {
      dataField: 'name',
      text: `${intl.formatMessage({ id: 'MANGA.MASTER.AUTHOR.NAME' })}`,
      ...SortColumn,
    },
    description: {
      dataField: 'description',
      text: `${intl.formatMessage({ id: 'MANGA.MASTER.AUTHOR.DESCRIPTION' })}`,
      formatter: (input: any) => <Tooltip children={<span>{formatLongString(input, 10, 10)}</span>} title={input}/>,
      ...SortColumn,
    },
    action: {
      dataField: 'action',
      text: `${intl.formatMessage({ id: 'MANGA.MASTER.CHAPTER.ACTION_COLUMN' })}`,
      formatter: ActionsColumnFormatter,
      formatExtraData: {
        intl,
        onShowDetail: (entity: any) => {
          history.push(`${window.location.pathname}/view/${entity.id}`);
        },
        onEdit: (entity: any) => {
          // if (["service-admin", "super-admin"].includes(userInfo.role)) {
          if (true) {
            setEditEntity(entity);
            history.push(`${window.location.pathname}/${entity.id}`);
          } else {
            setShowPermissionDeny(true);
          }
        },
        // onDelete: (entity: any) => {
        //   // if (["service-admin", "super-admin"].includes(userInfo.role)) {
        //   if(true) {
        //     setDeleteEntity(entity);
        //     setShowDelete(true);
        //   } else {
        //     setShowPermissionDeny(true)
        //   }
        // },
      },

    }
  }), []);

  const categoryModel = useMemo((): MasterBodyColumns => ({
    id: {
      dataField: 'id',
      text: `${intl.formatMessage({ id: 'MANGA.MASTER.AUTHOR.ID' })}`,
      ...SortColumn,
    },
    title: {
      dataField: 'title',
      text: `${intl.formatMessage({ id: 'MANGA.MASTER.AUTHOR.NAME' })}`,
      ...SortColumn,
    },
    description: {
      dataField: 'description',
      text: `${intl.formatMessage({ id: 'MANGA.MASTER.AUTHOR.DESCRIPTION' })}`,
      formatter: (input: any) => <Tooltip children={<span>{formatLongString(input, 10, 10)}</span>} title={input}/>,
      ...SortColumn,
    },
    action: {
      dataField: 'action',
      text: `${intl.formatMessage({ id: 'MANGA.MASTER.CHAPTER.ACTION_COLUMN' })}`,
      formatter: ActionsColumnFormatter,
      formatExtraData: {
        intl,
        onShowDetail: (entity: any) => {
          history.push(`${window.location.pathname}/view/${entity.id}`);
        },
        onEdit: (entity: any) => {
          // if (["service-admin", "super-admin"].includes(userInfo.role)) {
          if (true) {
            setEditEntity(entity);
            history.push(`${window.location.pathname}/${entity.id}`);
          } else {
            setShowPermissionDeny(true);
          }
        },
        // onDelete: (entity: any) => {
        //   // if (["service-admin", "super-admin"].includes(userInfo.role)) {
        //   if(true) {
        //     setDeleteEntity(entity);
        //     setShowDelete(true);
        //   } else {
        //     setShowPermissionDeny(true)
        //   }
        // },
      },

    }
  }), []);

  const createForm = useMemo((): ModifyForm => ({
    _header: createTitle,
    panel1: {
      _title: '',
      group1: group1,
    },
    // panel2: {
    //   _title: headerTitle,
    //   chapters: {
    //     _subTitle: 'EMPTY',
    //     _className: 'col-12 text-primary',
    //     '': {
    //       _type: 'custom',
    //       component: () => {
    //         return <DisplayTb name={'dataArray'} columns={tableModel}/>
    //       }
    //     },
    //   },
    // }
  }), [group1]);

  const updateForm = useMemo((): ModifyForm => ({
    _header: updateTitle,
    panel1: {
      _title: '',
      group1: editGroup1,
    },
    // panel2: {
    //   _title: headerTitle,
    //   table: {
    //     _subTitle: 'EMPTY',
    //     _className: 'col-12 text-primary',
    //     '': {
    //       _type: 'custom',
    //       component: () => {
    //         return <DisplayTb name={'dataArray'} columns={tableModel}/>
    //       }
    //     },
    //   },
    // }
  }), [editGroup1]);

  const viewForm = useMemo((): ModifyForm => ({
    _header: viewTitle,
    panel1: {
      _title: '',
      group1: viewGroup1,
      group2: viewGroup2,
    },
    panel2: {
      _title: "MANGA.MASTER.HEADER.AUTHOR",
      table: {
        _subTitle: 'EMPTY',
        _className: 'col-12 text-primary',
        '': {
          _type: 'custom',
          component: () => {
            return <DisplayTb name={'authors'} columns={authorModel}/>;
          }
        },
      },
    },
    panel3: {
      _title: "MANGA.MASTER.HEADER.CATEGORY",
      table: {
        _subTitle: 'EMPTY',
        _className: 'col-12 text-primary',
        '': {
          _type: 'custom',
          component: () => {
            return <DisplayTb name={'categories'} columns={categoryModel}/>;
          }
        },
      },
    },
    panel4: {
      _title: "MANGA.MASTER.HEADER.CHAPTER",
      table: {
        _subTitle: 'EMPTY',
        _className: 'col-12 text-primary',
        '': {
          _type: 'custom',
          component: () => {
            return <>
              <div className="row no-gutters mb-10">
                <button
                  type="button"
                  className="btn btn-primary mr-8"
                  onClick={onCreateChapter}>
                  <AddIcon style={iconStyle}/>
                  {intl.formatMessage({ id: createTitle ? createTitle : 'COMMON_COMPONENT.MASTER_BODY.HEADER.ADD_BTN' })}
                </button>
              </div>
              <DisplayTb name={'chapters'} columns={chapterModel}/>
            </>;
          }
        },
      },
    },

  }), [viewGroup1, viewGroup2]);

  const [chapter1, setChapter1] = useState<ModifyInputGroup>({
    _subTitle: '',
    // serviceId: {
    //   _type: 'string-alpha',
    //   label: 'MANGA.MASTER.TABLE.MANGA_ID',
    //   required: true,
    // },
    canonicalTitle: {
      _type: 'string',
      label: 'MANGA.MASTER.CHAPTER.CANONICAL_TITLE',
      required: true,
    },
    titleEn: {
      _type: 'string',
      label: 'MANGA.MASTER.CHAPTER.TITLE_EN',
      required: false,
    },
    titleVi: {
      _type: 'string',
      label: 'MANGA.MASTER.CHAPTER.TITLE_VI',
      required: false,
    },
    // translatedLanguage: {
    //   _type: 'search-select',
    //   label: 'RECOMMENDED.MASTER.TABLE.MANGA',
    //   onSearch: Chapter.Languages,
    //   keyField: 'name',
    //   required: true,
    //   onChange: (input: any) => input.name,
    // },
    number: {
      _type: 'string',
      label: 'MANGA.MASTER.CHAPTER.NUMBER',
      required: true,
    },
    thumbnailUrl: {
      // _type: 'string',
      _type: 'image',
      label: 'MANGA.MASTER.CHAPTER.THUMBNAIL_URL',
      isArray: false,
      maxNumber: 1,
    },
    pages: {
      // _type: 'string',
      _type: 'image',
      label: 'MANGA.MASTER.CHAPTER.PAGES',
      isArray: true,
      maxNumber: 100,
    },
    // type: {
    //   _type: 'radio',
    //   label: 'MANGA.MASTER.TABLE.MANGA',
    //   options: [
    //     { value: "SHARE", label: "MANGA.MANGA.OPTION_SHARE" },
    //     { value: "VIEW", label: "MANGA.MANGA.OPTION_VIEW" },
    //   ],
    //   required: true,
    // },
    // description: {
    //   _type: 'text-field',
    //   label: 'MANGA.MASTER.TABLE.NOTE',
    // },
    // icon: {
    //   _type: 'image',
    //   label: 'MANGA.MASTER.TABLE.IMAGE.RULE',
    //   maxNumber: 100,
    //   isArray: true,
    //   required: true,
    //
    // },
    // publicKey: {
    //   _type: 'string',
    //   required: true,
    //   label: 'MANGA.MASTER.TABLE.PUBLIC_KEY'
    // },
    // viewUrl: {
    //   _type: 'string',
    //   required: true,
    //   label: 'MANGA.MASTER.TABLE.URL_VIEW',
    // },
    // url: {
    //   required: true,
    //   _type: 'string',
    //   label: 'MANGA.MASTER.TABLE.URL_LOGIN',
    // },
  });

  const createChapterForm = useMemo((): ModifyForm => ({
    _header: createTitle,
    panel1: {
      _title: '',
      group1: chapter1,
    },
    // panel2: {
    //   _title: headerTitle,
    //   chapters: {
    //     _subTitle: 'EMPTY',
    //     _className: 'col-12 text-primary',
    //     '': {
    //       _type: 'custom',
    //       component: () => {
    //         return <DisplayTb name={'dataArray'} columns={tableModel}/>
    //       }
    //     },
    //   },
    // }
  }), [chapter1]);

  const actions: any = useMemo(() => ({
    type: 'inside',
    data: {
      save: {
        role: 'submit',
        type: 'submit',
        linkto: undefined,
        className: 'btn btn-primary mr-8 fixed-btn-width',
        label: 'SAVE_BTN_LABEL',
        icon: loading ? (<Spinner style={iconStyle} animation="border" variant="light" size="sm"/>) :
          (<SaveOutlinedIcon style={iconStyle}/>)
      },
      cancel: {
        role: 'link-button',
        type: 'button',
        linkto: '/service-management',
        className: 'btn btn-outline-primary fixed-btn-width',
        label: 'COMMON.BTN_CANCEL',
        icon: <CancelOutlinedIcon/>,
      }
    }
  }), [loading]);

  const onCreate = () => {
    history.push(`${window.location.pathname}/0000000`);
  };

  const onCreateChapter = () => {
    history.push(`${window.location.pathname}/chapter/0000000`);
  };

  return (
    <Fragment>
      <Switch>
        <Route path={`${HomePageURL.manga}/0000000`}>
          <EntityCrudPage
            mode={"vertical"}
            moduleName={"MODULE.NAME"}
            onModify={add}
            formModel={createForm}
            entity={createEntity}
            actions={actions}
            formatter={(entity: any) => {
              return {
                ...entity,
                titles: { en: entity.titleEn, vi: entity.titleVi, ja: entity.titleJa },
                authors: [entity.authors],
                categories: [entity.categories],
              };
            }}
            submitSuccessMsg={"MANGA.ADD_MANGA.SUCCESS_MSG"}
            homePageUrl={HomePageURL.manga}
            backDetail={"serviceId"}
          />
        </Route>
        <Route path={`${HomePageURL.manga}/view/:code/chapter/0000000`}>
          {({ match }) => (
          <EntityCrudPage
            mode={"vertical"}
            moduleName={"MODULE.NAME"}
            onModify={async (createEntity) => {
              await Chapter.Create(createEntity);
              history.goBack();
            }
            }
            formModel={createChapterForm}
            entity={createEntity}
            actions={actions}
            formatter={(entity: any) => {
              return {
                ...entity,
                mangaId: `${match?.params.code}`,
                titles: { en: entity.titleEn, vi: entity.titleVi },
                translatedLanguage: "en",
                originalLanguage: "ja",
                // icon: entity.icon?.path ?? entity.icon,
                // providerId: entity.providerId?.providerId ?? entity.providerId
              };
            }}
            submitSuccessMsg={"MANGA.ADD_CHAPTER.SUCCESS_MSG"}
            homePageUrl={HomePageURL.manga}
            backDetail={"serviceId"}
          />)}
        </Route>
        <Route path={`${HomePageURL.manga}/view/:code`}>
          {({ match }) => (
            <EntityCrudPage
              mode={"vertical"}
              onModify={update}
              setEditEntity={setEditEntity}
              moduleName={"MODULE.NAME"}
              code={match?.params.code}
              get={GetById}
              formModel={viewForm}
              formatter={(entity: any) => {
                return {
                  ...entity,
                  icon: entity.icon?.path ?? entity.icon,
                  providerId: entity.providerId?.providerId ?? entity.providerId
                };
              }}
              homePageUrl={HomePageURL.manga}
              backHome={true}
              actions={viewActions}
            />
          )}
        </Route>
        <Route path={`${HomePageURL.manga}/:code`}>
          {({ match }) => (
            <EntityCrudPage
              mode={"vertical"}
              onModify={update}
              setEditEntity={setEditEntity}
              moduleName={"MODULE.NAME"}
              code={match?.params.code}
              get={GetById}
              formModel={updateForm}
              actions={actions}
              formatter={(entity: any) => {
                return {
                  ...entity,
                  titles: { en: entity.titleEn, vi: entity.titleVi, ja: entity.titleJa },
                  authors: [entity.authors],
                  categories: [entity.categories],
                  // icon: entity.icon?.path ?? entity.icon,
                  // providerId: entity.providerId?.providerId ?? entity.providerId
                };
              }}
              submitSuccessMsg={"MANGA.UPDATE_MANGA.SUCCESS_MSG"}
              homePageUrl={HomePageURL.manga}
              backDetail={"serviceId"}
              backHome={true}
            />
          )}
        </Route>
        <Route path={`${HomePageURL.manga}`} exact={true}>
          <MasterHeader
            title={headerTitle}
            onSearch={(value) => {
              setPaginationProps(DefaultPagination);
              setFilterProps(value);
            }}
            searchModel={searchModel}
          />
          <div className="activity-body">
            <MasterBody
              title="MANGA.MASTER.TABLE.TITLE"
              optionTitle={Object.keys(filterProps).length > 0 ? (total > 0 ? `${total} ${intl.formatMessage({ id: "COMMON_SEARCH.RESULT.RESULT" })}` : intl.formatMessage({ id: "COMMON_SEARCH.RESULT.NO_RESULT" })) : ""}
              selectedEntities={selectedEntities}
              onCreate={onCreate}
              createTitle={createTitle}
              entities={entities}
              total={total}
              columns={columns as any}
              loading={loading}
              paginationParams={paginationProps}
              setPaginationParams={setPaginationProps}
              isShowId={false}
            />
          </div>
        </Route>
      </Switch>
      <DeleteEntityDialog
        entity={deleteEntity}
        onDelete={deleteFn}
        deleteSuccessToast={"PROVIDER.DELETE.DELETE_SUCCESS"}
        isShow={showDelete}
        loading={loading}
        error={error}
        onHide={() => {
          setShowDelete(false);
        }}
        title={"PROVIDER.DELETE.TITLE"}
        confirmMessage={"PROVIDER.DELETE.MSG"}
        bodyTitle={"PROVIDER.DELETE.BODY_MSG"}
        deletingMessage={" "}
        cancelBtn={"COMMON.BTN_CANCEL"}
        deleteBtn={"PROVIDER.DELETE.DELETE_BTN"}
      />
      <NotifyDialog
        title={"COMMON.AUTH.NOT_HAVE_PERMISSION.TITLE"}
        notifyMessage={" "} isShow={showPermissionDeny}
        onHide={() => setShowPermissionDeny(false)}
      />
    </Fragment>
  );
}

export default Manga;
