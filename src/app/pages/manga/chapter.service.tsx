import axios from 'axios';
import { API_BASE_URL } from '../../common-library/common-consts/enviroment';
import {
  CountProps,
  CreateProps,
  DeleteManyProps,
  DeleteProps,
  GetAllPropsServer,
  GetProps,
  UpdateProps,
} from '../../common-library/common-types/common-type';
import { formatParamsGet } from '../../common-library/helpers/format-params';
import firebase from "firebase/compat";

export const API_URL = API_BASE_URL + `/manga`;

export const BULK_API_URL = API_URL + '/bulk'

export const Create: CreateProps<any> = async (data: any) => {
  const storage = firebase.storage();
  const ref = storage.ref();
  let pages: string[] = [];
  let thumbnailUrl;

  console.log(data.pages);
  console.log(data.thumbnailUrl);

  await Promise.all(data.pages.map( async (v:any, i: number) => {
    const imgRef = ref.child("manga").child(data.mangaId).child(data.number).child(String(i)).put(v.file)
    imgRef.on('state_changed', function (snapshot) {
    }, function (error) {
      console.log(error);
    }, async function () {
      // Uploaded completed successfully, now we can get the download URL

    });
    await imgRef;
    await imgRef.snapshot.ref.getDownloadURL().then(function (downloadURL) {
      //getting url of image
      pages.push(downloadURL);
    });
  }))
  const imgRef = ref.child("manga").child(data.mangaId).child(data.number).put(data.thumbnailUrl.file)
  imgRef.on('state_changed', function (snapshot) {
  }, function (error) {
    console.log(error);
  }, async function () {
    // Uploaded completed successfully, now we can get the download URL

  });
  await imgRef;
  await imgRef.snapshot.ref.getDownloadURL().then(function (downloadURL) {
    //getting url of image
    thumbnailUrl = downloadURL;
  });

  return axios.post(`${API_URL}/${data.mangaId}/chapters`, {...data, pages, thumbnailUrl});
};

export const Languages: CountProps<any> = (queryProps) => {
  return axios.get(`${API_BASE_URL}/languages`, {
    params: { ...queryProps },
  });
};

export const GetAll: GetAllPropsServer<any> = ({
  queryProps,
  sortList,
  paginationProps,
}) => {
  return axios.get(`${API_URL}`, {
    params: formatParamsGet(queryProps, sortList, paginationProps, true),
  });
};

export const Count: CountProps<any> = (queryProps) => {
  return axios.get(`${API_URL}/get/count`, {
    params: { ...queryProps },
  });
};

export const GetById = (_id: string) => {
  return axios.get(`${API_URL}/${_id}`);
};

export const Get: GetProps<any> = (entity) => {
  return axios.get(`${API_URL}/${entity.id}`);
};

export const Update: UpdateProps<any> = (entity: any) => {
  return axios.put(`${API_URL}/${entity.id}`, entity);
};

export const Delete: DeleteProps<any> = (entity: any) => {
  return axios.delete(`${API_URL}/${entity.id}`);
};

export const DeleteMany: DeleteManyProps<any> = (entities: any[]) => {
  return axios.delete(BULK_API_URL, {
    data: { data: entities },
  });
};

