import React, { Fragment, useEffect, useMemo, useState } from 'react';
import { FormattedDate, FormattedMessage, FormattedTime, useIntl } from 'react-intl';
import { DefaultPagination, HomePageURL } from '../../common-library/common-consts/const';
import { MasterHeader } from '../../common-library/common-components/master-header';
import {
  SearchModel
} from '../../common-library/common-types/common-type';
import { InitMasterProps } from '../../common-library/helpers/common-function';
import { Route, Switch } from 'react-router-dom';
import { Count, Create, Delete, DeleteMany, Get, GetAll, Update } from "./activity.service";
import { MasterBody } from "../../common-library/common-components/master-body";
import { NotifyDialog } from '../../common-library/common-components/notify-dialog';
import { Tooltip } from '@material-ui/core';

const headerTitle = 'ACTIVITY.MASTER.HEADER.TITLE';
const tableTitle = 'ACTIVITY.MASTER.TABLE.TITLE';

function Activity() {
  const intl = useIntl();

  const {
    entities,
    setEntities,
    deleteEntity,
    setDeleteEntity,
    createEntity,
    setCreateEntity,
    selectedEntities,
    setSelectedEntities,
    detailEntity,
    showDelete,
    setShowDelete,
    showDeleteMany,
    setShowDeleteMany,
    showDetail,
    setShowDetail,
    paginationProps,
    setPaginationProps,
    filterProps,
    setFilterProps,
    total,
    loading,
    setLoading,
    error,
    add,
    update,
    get,
    deleteMany,
    deleteFn,
    convertUnixTimeToDate,
    getAll,
    formatLongString,
  } = InitMasterProps<any>({
    getServer: Get,
    countServer: Count,
    createServer: Create,
    deleteServer: Delete,
    deleteManyServer: DeleteMany,
    getAllServer: GetAll,
    updateServer: Update,
  });

  const [currentTab, setCurrentTab] = useState<string | undefined>('0');
  const [showPermissionDeny, setShowPermissionDeny] = useState<boolean>(false);
  const [trigger, setTrigger] = useState<boolean>(false);
  const [fromUser, setFromUser] = useState<string>("");

  useEffect(() => {
    const search = window?.location?.search;
    const param = search.replace('?', '').split('=');
    if (param?.length > 1) {
      getAll({ ...filterProps, [param[0]]: param[1] })
      setFromUser(param[1]);
    } else {
      setFromUser("");
      getAll(filterProps);
    }
  }, [paginationProps, filterProps, trigger, currentTab]);

  const columns = useMemo(() => {
    return [
      {
        dataField: 'action',
        text: `${intl.formatMessage({ id: 'ACTIVITY.MASTER.TABLE.ACTION' })}`,
        headerClasses: 'text-center',
        formatter: (input: any) => <FormattedMessage id={`ACTIVITY.ACTION_TYPE.${input.toString().toUpperCase()}`} />,
        align: 'center',
      },
      {
        dataField: 'fromUser',
        text: `${intl.formatMessage({ id: 'ACTIVITY.MASTER.TABLE.FROM_USER' })}`,
        headerClasses: 'text-center',
        formatter: (input: any) => <Tooltip children={<span>{formatLongString(input, 6, 6)}</span>} title={input} />,
        align: 'center',
      },
      {
        dataField: 'toUser',
        text: `${intl.formatMessage({ id: 'ACTIVITY.MASTER.TABLE.TO_USER' })}`,
        formatter: (input: any) => <Tooltip children={<span>{formatLongString(input, 6, 6)}</span>} title={input} />,
        headerClasses: 'text-center',
        align: 'center',
      },
      {
        dataField: 'serviceId',
        text: `${intl.formatMessage({ id: 'SERVICE.MASTER.TABLE.SERVICE_ID' })}`,
        align: 'center',
        formatter: (input: any) => <Tooltip children={<span>{formatLongString(input, 10, 10)}</span>} title={input} />,
        headerClasses: 'text-center',
      },
      {
        dataField: 'data',
        text: `${intl.formatMessage({ id: 'ACTIVITY.MASTER.TABLE.DATA' })}`,
        align: 'center',
        formatter: (input: any) => <Tooltip children={<span>{formatLongString(input, 10, 10)}</span>} title={input} />,
        headerClasses: 'text-center',
      },
      {
        dataField: 'date',
        text: `${intl.formatMessage({ id: 'ACTIVITY.MASTER.TABLE.DATE' })}`,
        formatter: (input: any) => <><FormattedDate value={convertUnixTimeToDate(input)} /> <FormattedTime value={convertUnixTimeToDate(input)} /></>,
        headerClasses: 'text-center',
        align: 'center',
      },
      {
        dataField: 'device',
        text: `${intl.formatMessage({ id: 'ACTIVITY.MASTER.TABLE.DEVICE' })}`,
        formatter: (input: any) => <Tooltip children={<span>{formatLongString(input, 28, 28)}</span>} title={input} />,
        align: 'center',
        headerClasses: 'text-center',
      },
    ]
  }, []);

  const searchModel: SearchModel = useMemo(() => ({
    fromUser: {
      type: 'string',
      label: 'ACTIVITY.MASTER.SEARCH.FROM_USER',
      placeholder: 'ACTIVITY.MASTER.SEARCH.FROM_USER',
      className: 'col-sm-4 col-xxl-2 col-md-2 master-header-search-input-margin',
      defaultValue: fromUser
    },
    toUser: {
      type: 'string',
      label: 'ACTIVITY.MASTER.SEARCH.TO_USER',
      placeholder: 'ACTIVITY.MASTER.SEARCH.TO_USER',
      className: 'col-sm-4 col-xxl-2 col-md-2 master-header-search-input-margin'
    },
    fromDate: {
      type: 'date-time',
      showTime: true,
      label: 'ACTIVITY.MASTER.SEARCH.FROM_TIME',
      placeholder: 'ACTIVITY.MASTER.SEARCH.FROM_TIME',
      className: 'col-sm-4 col-xxl-2 col-md-2 master-header-search-input-margin'
    },
    toDate: {
      type: 'date-time',
      showTime: true,
      label: 'ACTIVITY.MASTER.SEARCH.TO_TIME',
      placeholder: 'ACTIVITY.MASTER.SEARCH.TO_TIME',
      className: 'col-sm-4 col-xxl-2 col-md-2 master-header-search-input-margin'
    },
  }), [currentTab, fromUser]);

  return (
    <Fragment>
      <Switch>
        <Route path={`${HomePageURL.activity}`} exact={true}>
          <MasterHeader
            title={headerTitle}
            onSearch={(value) => {
              setPaginationProps(DefaultPagination)
              setFilterProps(value)
            }}
            path={'/activity'}
            searchModel={searchModel}
          />
          <div className="activity-body">
            <MasterBody
              title='ACTIVITY.MASTER.TABLE.TITLE'
              optionTitle={Object.keys(filterProps).length > 0 ? (total > 0 ? `${total} ${intl.formatMessage({ id: "COMMON_SEARCH.RESULT.RESULT" })}` : intl.formatMessage({ id: "COMMON_SEARCH.RESULT.NO_RESULT" })) : ""}
              selectedEntities={selectedEntities}
              entities={entities}
              total={total}
              columns={columns as any}
              loading={loading}
              paginationParams={paginationProps}
              setPaginationParams={setPaginationProps}
              isShowId={false}
            />
          </div>
        </Route>
      </Switch>
      <NotifyDialog
        title={"COMMON.AUTH.NOT_HAVE_PERMISSION.TITLE"}
        notifyMessage={" "} isShow={showPermissionDeny}
        onHide={() => setShowPermissionDeny(false)}
      />
    </Fragment>
  );
}

export default Activity
