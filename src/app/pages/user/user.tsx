import React, { Fragment, useEffect, useMemo, useState } from 'react';
import { FormattedDate, useIntl, FormattedTime } from 'react-intl';
import { dangerIconStyle, DefaultPagination, HomePageURL, NormalColumn, successIconStyle, iconStyle } from '../../common-library/common-consts/const';
import { MasterHeader } from '../../common-library/common-components/master-header';
import {
  ActionsColumnFormatter,
} from '../../common-library/common-components/actions-column-formatter';
import { DeleteEntityDialog } from '../../common-library/common-components/delete-entity-dialog';
import {
  ModifyForm,
  ModifyInputGroup,
  SearchModel
} from '../../common-library/common-types/common-type';
import { InitMasterProps, InitValues, } from '../../common-library/helpers/common-function';
import { Route, Switch, useHistory } from 'react-router-dom';
import EntityCrudPage from '../../common-library/common-components/entity-crud-page';
import { BanUser, Count, Create, Delete, DeleteMany, Get, GetAll, GetById, UnbanUser, Update } from "./user.service";
import './style.scss'
import { MasterBody } from "../../common-library/common-components/master-body";
import { AssignmentOutlined, BlockOutlined, CheckOutlined, Edit } from '@material-ui/icons';
import { NotifyDialog } from '../../common-library/common-components/notify-dialog';
import { Tooltip } from '@material-ui/core';
import { RootStateOrAny, useSelector } from 'react-redux'; import { Spinner } from 'react-bootstrap';
import SaveOutlinedIcon from '@material-ui/icons/SaveOutlined';
import { DisplayImage } from "../../common-library/helpers/detail-helpers";

const headerTitle = 'USER.MASTER.HEADER.TITLE';
const moduleName = 'USER.MASTER.HEADER.TITLE';
const lockDialogTitle = 'USER.LOCK_DIALOG.TITLE';
const unlockDialogTitle = 'USER.UNLOCK_DIALOG.TITLE';
const lockBtn = 'USER.LOCK_DIALOG.LOCK_BTN';
const unlockBtn = 'USER.UNLOCK_DIALOG.UNLOCK_BTN';
const lockConfirmMessage = 'USER.LOCK_DIALOG.CONFIRM_MESSAGE';
const lockDialogBodyTitle = 'USER.LOCK_DIALOG.BODY_TITLE';
const createTitle = "USER.MASTER.TABLE.TITLE";
const updateTitle = "USER.MASTER.TABLE.TITLE";
const viewTitle = 'USER.MASTER.TABLE.TITLE';

function User() {
  const intl = useIntl();

  const history = useHistory();
  const {
    entities,
    setEntities,
    deleteEntity,
    setDeleteEntity,
    createEntity,
    setCreateEntity,
    selectedEntities,
    setSelectedEntities,
    detailEntity,
    showDelete,
    setShowDelete,
    editEntity,
    setEditEntity,
    showDeleteMany,
    setShowDeleteMany,
    showDetail,
    setShowDetail,
    paginationProps,
    setPaginationProps,
    filterProps,
    setFilterProps,
    total,
    loading,
    setLoading,
    error,
    add,
    update,
    get,
    deleteMany,
    deleteFn,
    convertUnixTimeToDate,
    getAll,
    formatLongString,
  } = InitMasterProps<any>({
    getServer: GetById,
    countServer: Count,
    createServer: Create,
    deleteServer: Delete,
    deleteManyServer: DeleteMany,
    getAllServer: GetAll,
    updateServer: Update,
  });

  const [currentTab, setCurrentTab] = useState<string | undefined>('0');
  const [showPermissionDeny, setShowPermissionDeny] = useState<boolean>(false);
  const [trigger, setTrigger] = useState<boolean>(false);
  const [showToggleConfirm, setToggleConfirmDialog] = useState<boolean>(false);
  const [role, setRole] = useState<string>("");
  const userInfo = useSelector(({ auth }: { auth: RootStateOrAny }) => auth);
  const [canChange, setCanChangeStatus] = useState<boolean>(true)
  const [onChange, setOnChange] = useState<boolean>(false);


  useEffect(() => {
    getAll(filterProps);
  }, [paginationProps, filterProps, trigger, currentTab]);

  const columns = useMemo(() => {
    return [
      {
        dataField: 'id',
        text: `${intl.formatMessage({ id: 'USER.MASTER.TABLE.ID' })}`,
        headerClasses: 'text-center',
        align: 'center',
      },
      {
        dataField: 'username',
        text: `${intl.formatMessage({ id: 'USER.MASTER.TABLE.USERNAME' })}`,
        headerClasses: 'text-center',
        align: 'center',
      },
      {
        dataField: 'email',
        text: `${intl.formatMessage({ id: 'USER.MASTER.TABLE.MAIL' })}`,
        formatter: (input: any) => <Tooltip children={<span>{formatLongString(input, 12, 12)}</span>} title={input} />,
        headerClasses: 'text-center',
        align: 'center',
      },
      {
        dataField: 'avatarUrl',
        class: "btn-primary",
        text: `${intl.formatMessage({ id: 'USER.MASTER.TABLE.ROLE' })}`,
        headerClasses: 'text-center',
        classes: 'text-center',
        formatter:DisplayImage,
      },
      // {
      //   dataField: 'createdAt',
      //   text: `${intl.formatMessage({ id: 'USER.ACCOUNT_INFO.JOIN_DATE' })}`,
      //   formatter: (input: any) => <FormattedDate value={convertUnixTimeToDate(input)} />,
      //   headerClasses: 'text-center',
      //   align: 'center',
      // },
      {
        dataField: 'role',
        class: "btn-primary",
        text: `${intl.formatMessage({ id: 'USER.MASTER.TABLE.ROLE' })}`,
        headerClasses: 'text-center',
        classes: 'text-center',
      },
      {
        dataField: 'createdAt',
        text: `${intl.formatMessage({ id: 'USER.ACCOUNT_INFO.JOIN_DATE' })}`,
        // formatter: (input: any) => <FormattedDate value={convertUnixTimeToDate(input)} />,
        headerClasses: 'text-center',
        align: 'center',
      },
      {
        dataField: 'updatedAt',
        text: `${intl.formatMessage({ id: 'USER.ACCOUNT_INFO.JOIN_DATE' })}`,
        // formatter: (input: any) => <FormattedDate value={convertUnixTimeToDate(input)} />,
        headerClasses: 'text-center',
        align: 'center',
      },

      {
        dataField: 'action',
        text: `${intl.formatMessage({ id: 'USER.MASTER.TABLE.ACTION_COLUMN' })}`,
        formatter: ActionsColumnFormatter,
        formatExtraData: {
          intl,
          onEdit: (entity: any) => {
            // if (["super-admin", "user-admin"].includes(userInfo.role)) {
            if (true) {
              setEditEntity(entity)
              history.push(`${window.location.pathname}/${entity.id}`);
            } else {
              setShowPermissionDeny(true);
            }
          },
          onShowDetail: (entity: any) => {
            history.push(`${window.location.pathname}/view/${entity.id}`);
          },
          // onLock: (entity: any) => {
          //   if (["super-admin", "user-admin"].includes(userInfo.role)) {
          //     setDeleteEntity(entity)
          //     setShowDelete(true);
          //   } else {
          //     setShowPermissionDeny(true);
          //   }
          // },
        },
        ...NormalColumn,
        style: { minWidth: '130px' },
      }
    ]
  }, []);

  const searchModel: SearchModel = useMemo(() => ({
    username: {
      type: 'string',
      label: 'USER.MASTER.SEARCH.USERNAME',
      placeholder: 'USER.MASTER.SEARCH.USERNAME'
    },
    // kanji: {
    //   type: 'string',
    //   label: 'USER.MASTER.SEARCH.KANJI',
    //   placeholder: 'USER.MASTER.SEARCH.KANJI'
    // },
    // katakana: {
    //   type: 'string',
    //   label: 'USER.MASTER.SEARCH.ROMANJI',
    //   placeholder: 'USER.MASTER.SEARCH.ROMANJI'
    // },
    email: {
      type: 'string',
      label: 'USER.MASTER.SEARCH.MAIL',
      placeholder: 'USER.MASTER.SEARCH.MAIL',
    },
    // isBanned: {
    //   type: 'search-select',
    //   onSearch: () => (
    //     {
    //       "data": [{ value: "admin", label: intl.formatMessage({ id: 'USER.OPTION.ACTIVE' }) },
    //       { value: "user", label: intl.formatMessage({ id: 'USER.OPTION.BANNED' }) }]
    //     }),
    //   label: 'USER.MASTER.SEARCH.ISBANNED',
    //   placeholder: 'USER.MASTER.SEARCH.ISBANNED',
    // },
    // role: {
    //   type: 'search-select',
    //   onSearch: () => (
    //     {
    //       "data": [{ value: "admin", label: intl.formatMessage({ id: 'USER.OPTION.ACTIVE' }) },
    //         { value: "user", label: intl.formatMessage({ id: 'USER.OPTION.BANNED' }) }]
    //     }),
    //   label: 'USER.MASTER.SEARCH.ISBANNED',
    //   placeholder: 'USER.MASTER.SEARCH.ISBANNED',
    // },
    // fromDate: {
    //   type: 'date-time',
    //   label: 'ACTIVITY.MASTER.SEARCH.FROM_TIME',
    //   placeholder: 'ACTIVITY.MASTER.SEARCH.FROM_TIME',
    // },
    // toDate: {
    //   type: 'date-time',
    //   label: 'ACTIVITY.MASTER.SEARCH.TO_TIME',
    //   placeholder: 'ACTIVITY.MASTER.SEARCH.TO_TIME',
    // },
  }), [currentTab]);

  const updateGroup1 = useMemo((): ModifyInputGroup => ({
    _subTitle: 'USER.ACCOUNT_INFO.INFO',
    _className: 'col-6 pr-xl-15 pr-md-10 pr-5',
    role: {
      // _type: 'search-select',
      _type: 'string',
      required: true,
      disabled: true,
      label: 'USER.MASTER.SEARCH.ROLE',
      // onSearch: () => (
      //   {
      //     "data": [
      //       { label: intl.formatMessage({ id: 'USER.OPTION.ADMIN' }), value: "admin" },
      //       { label: intl.formatMessage({ id: 'USER.OPTION.USER' }), value: "user" },
      //     ]
      //   }
      // ),
      // onChange: (value) => {
      //   console.log(value);
      //   setRole(value?.value || "user")
      //   setCanChangeStatus(!canChange)
      //   setOnChange(!onChange)
      // }
    },
    username: {
      _type: 'string',
      required: true,
      // disabled: true,
      label: "USER.ACCOUNT_INFO.USERNAME",
    },
    // kanji: {
    //   _type: 'string-kanji',
    //   label: 'USER.ACCOUNT_INFO.KANJI',
    //   required: true,
    // },
    // katakana: {
    //   _type: 'string-katakana',
    //   required: true,
    //   label: 'USER.ACCOUNT_INFO.KATAKANA',
    // },
    // birthday: {
    //   _type: 'date-time',
    //   required: true,
    //   label: 'USER.ACCOUNT_INFO.DOB',
    //   // formatter: (time: any) => {
    //   //   return time - new Date().getTimezoneOffset() * 60000;
    //   // }
    //   // disabled: true,
    // },
    email: {
      _type: 'email',
      required: true,
      label: 'USER.ACCOUNT_INFO.EMAIL',
    },
    avatarUrl: {
      _type: 'string',
      required: true,
      label: 'USER.ACCOUNT_INFO.AVATAR',
      // formatter: DisplayImage,
    },
    createdAt: {
      _type: 'date-time',
      label: 'USER.ACCOUNT_INFO.JOIN_DATE',
      disabled: true,
    },
    updatedAt: {
      _type: 'date-time',
      label: 'USER.ACCOUNT_INFO.JOIN_DATE',
      disabled: true,
    },
    // pid: {
    //   _type: 'string-citizen',
    //   required: true,
    //   label: 'USER.ACCOUNT_INFO.CID',
    // }
  }), [canChange, onChange]);

  const group1 = useMemo((): ModifyInputGroup => ({
    _subTitle: 'USER.ACCOUNT_INFO.INFO',
    _className: 'col-6 pr-xl-15 pr-md-10 pr-5',
    // role: {
    //   // _type: 'search-select',
    //   _type: 'string',
    //   required: true,
    //   disabled: true,
    //   label: 'USER.MASTER.SEARCH.ROLE',
    //   // onSearch: () => (
    //   //   {
    //   //     "data": [
    //   //       { label: intl.formatMessage({ id: 'USER.OPTION.ADMIN' }), value: "admin" },
    //   //       { label: intl.formatMessage({ id: 'USER.OPTION.USER' }), value: "user" },
    //   //     ]
    //   //   }
    //   // ),
    //   // onChange: (value) => {
    //   //   console.log(value);
    //   //   setRole(value?.value || "user")
    //   //   setCanChangeStatus(!canChange)
    //   //   setOnChange(!onChange)
    //   // }
    // },
    username: {
      _type: 'string',
      required: true,
      // disabled: true,
      label: "USER.ACCOUNT_INFO.USERNAME",
    },
    // kanji: {
    //   _type: 'string-kanji',
    //   label: 'USER.ACCOUNT_INFO.KANJI',
    //   required: true,
    // },
    // katakana: {
    //   _type: 'string-katakana',
    //   required: true,
    //   label: 'USER.ACCOUNT_INFO.KATAKANA',
    // },
    // birthday: {
    //   _type: 'date-time',
    //   required: true,
    //   label: 'USER.ACCOUNT_INFO.DOB',
    //   // formatter: (time: any) => {
    //   //   return time - new Date().getTimezoneOffset() * 60000;
    //   // }
    //   // disabled: true,
    // },
    email: {
      _type: 'email',
      required: true,
      label: 'USER.ACCOUNT_INFO.EMAIL',
    },
    password: {
      _type: 'string',
      required: true,
      label: 'USER.ACCOUNT_INFO.PASSWORD',
    },
    avatarUrl: {
      _type: 'string',
      required: true,
      label: 'USER.ACCOUNT_INFO.AVATAR',
    },
    // createdAt: {
    //   _type: 'date-time',
    //   label: 'USER.ACCOUNT_INFO.JOIN_DATE',
    //   disabled: true,
    // },
    // updatedAt: {
    //   _type: 'date-time',
    //   label: 'USER.ACCOUNT_INFO.JOIN_DATE',
    //   disabled: true,
    // },
    // pid: {
    //   _type: 'string-citizen',
    //   required: true,
    //   label: 'USER.ACCOUNT_INFO.CID',
    // }
  }), [canChange, onChange]);

  const viewGroup1 = useMemo((): ModifyInputGroup => ({
    _subTitle: 'USER.ACCOUNT_INFO.INFO',
    _className: 'col-6 pr-xl-15 pr-md-10 pr-5',
    role: {
      _type: 'string',
      // label: 'USER.ACCOUNT_INFO.STATUS',
      // disabled: true,
      // formatter: (input: any) => {
      //   return input ? intl.formatMessage({ id: 'USER.OPTION.BANNED' }) : intl.formatMessage({ id: 'USER.OPTION.ACTIVE' })
      // }

      // _type: 'search-select',
      label: 'USER.MASTER.SEARCH.ROLE',
      disabled: true,
      // onSearch: () => (
      //   {
      //     "data": [
      //       { label: intl.formatMessage({ id: 'USER.OPTION.ADMIN' }), value: "admin" },
      //       { label: intl.formatMessage({ id: 'USER.OPTION.USER' }), value: "user" },
      //     ]
      //   }
      // ),
    },
    username: {
      _type: 'string',
      disabled: true,
      label: "USER.ACCOUNT_INFO.USERNAME",
    },
    // kanji: {
    //   _type: 'string',
    //   label: 'USER.ACCOUNT_INFO.KANJI',
    //   disabled: true,
    // },
    // katakana: {
    //   _type: 'string',
    //   label: 'USER.ACCOUNT_INFO.KATAKANA',
    //   disabled: true,
    // },
    // birthday: {
    //   _type: 'date-time',
    //   label: 'USER.ACCOUNT_INFO.DOB',
    //   disabled: true,
    //   // formatter: (time: any) => {
    //   //   return time - new Date().getTimezoneOffset() * 60000;
    //   // }
    // },
    email: {
      _type: 'string',
      label: 'USER.ACCOUNT_INFO.EMAIL',
      disabled: true,
    },
    avatarUrl: {
      _type: 'string',
      label: 'USER.ACCOUNT_INFO.AVATAR',
      disabled: true,
    },
    createdAt: {
      _type: 'date-time',
      label: 'USER.ACCOUNT_INFO.JOIN_DATE',
      disabled: true,
    },
    updatedAt: {
      _type: 'date-time',
      label: 'USER.ACCOUNT_INFO.JOIN_DATE',
      disabled: true,
    },
    // pid: {
    //   _type: 'string',
    //   label: 'USER.ACCOUNT_INFO.CID',
    //   disabled: true,
    // }
  }), []);

  // const group2 = useMemo((): ModifyInputGroup => ({
  //   _subTitle: "USER.ACCOUNT_INFO.SERVICE",
  //   _className: 'col-6 pl-xl-15 pl-md-10 pl-5',
  //   services: {
  //     _type: 'custom',
  //     component: (data) => {
  //       const services = data?.values?.services;
  //       return services?.length > 0 ? <>{services.map((e: any) => {
  //         return (
  //           <div className="mb-4">
  //             <img src={e.icon} alt="icon" width="32px" />
  //
  //             <Tooltip className="ml-2" children={<span>{formatLongString(e.name.toString().toUpperCase(), 10, 10)}</span>} title={e.name.toString().toUpperCase()} />,
  //
  //             <span className="ml-6">
  //               <FormattedDate value={convertUnixTimeToDate(e.joinDate)} />
  //               <span className="ml-2"><FormattedTime value={convertUnixTimeToDate(e.joinDate)} /></span>
  //             </span>
  //           </div>
  //         );
  //       })}</> : <>{intl.formatMessage({ id: 'USER.ACCOUNT_INFO.NO_SERVICE' })}</>
  //     },
  //     label: 'USER.ACCOUNT_INFO.DATA',
  //   }
  // }), []);

  const createForm = useMemo((): ModifyForm => ({
    _header: createTitle,
    panel1: {
      _title: '',
      group1: group1,
      // group2: group2,
    },
  }), [group1]);

  const updateForm = useMemo((): ModifyForm => ({
    _header: updateTitle,
    panel1: {
      _title: '',
      group1: updateGroup1,
      // group2: group2,
    },
  }), [updateGroup1]);

  // const updateForm = useMemo((): ModifyForm => {
  //   return ({ ...createForm, _header: updateTitle });
  // }, [createForm]);

  const viewForm = useMemo((): ModifyForm => ({
    _header: viewTitle,
    panel1: {
      _title: '',
      group1: viewGroup1,
      // group2: group2,
    },
  }), [viewGroup1]);

  const viewActions: any = useMemo(() => ({
    type: 'inside',
    data: {
      edit: {
        role: 'link-button',
        type: 'link',
        linkto: `/user-management`,
        queryID: true,
        className: 'ml-8 btn btn-primary fixed-btn-width',
        label: 'COMMON.BTN_EDIT_USER',
        icon: <Edit />,
        reload: true,
        // guard: ["super-admin", "user-admin"].includes(userInfo.role),
      },
      // detailActivity: {
      //   role: 'link-button',
      //   type: 'link',
      //   linkto: '/activity',
      //   optionParam: "fromUser",
      //   optionField: "username",
      //   className: 'ml-8 btn btn-outline-secondary',
      //   label: 'COMMON.BTN_ROUTE_ACTIVITY',
      //   icon: <AssignmentOutlined />,
      //   guard: true,
      // },
    }
  }), [loading]);

  const actions: any = useMemo(() => ({
    type: 'inside',
    data: {
      save: {
        id: 'save-user',
        role: 'submit',
        type: 'submit',
        linkto: undefined,
        className: 'btn btn-primary fixed-btn-width',
        label: 'SAVE_BTN_LABEL',
        icon: loading ? (<Spinner style={iconStyle} animation="border" variant="light" size="sm" />) :
          (<SaveOutlinedIcon style={iconStyle} />)
      },
      // cancel: {
      //   role: 'link-button',
      //   type: 'button',
      //   linkto: '/user-management',
      //   className: 'btn btn-outline-primary fixed-btn-width',
      //   label: 'COMMON.BTN_CANCEL',
      //   icon: <CancelOutlinedIcon />,
      //   guard: true,
      // },
      // detailActivity: {
      //   role: 'link-button',
      //   type: 'link',
      //   linkto: '/activity',
      //   optionParam: "fromUser",
      //   optionField: "username",
      //   className: 'ml-8 btn btn-outline-secondary',
      //   label: 'COMMON.BTN_ROUTE_ACTIVITY',
      //   icon: <AssignmentOutlined />,
      //   guard: true,
      // },

    }
  }), [loading]);

  const onCreate = () => {
    history.push(`${window.location.pathname}/0000000`);
  }

  return (
    <Fragment>
      <Switch>
        <Route path={`${HomePageURL.user}/0000000`}>
          <EntityCrudPage
            setEditEntity={setEditEntity}
            onModify={add}
            moduleName={moduleName}
            formModel={createForm}
            actions={actions}
            homePageUrl={HomePageURL.user}
            optionHeaderField="kanji"
            submitSuccessMsg={"USER.UPDATE_USER.SUCCESS_MSG"}
            backDetail={"id"}
            backHome={true}
            onSecondModify={async (e) => {
              if (onChange) {
                // const result = isBanned ? await BanUser(e) : await UnbanUser(e);
                // if (!result.error) setTrigger(!trigger)
                setOnChange(false)
                setRole("user")
                // return result;
              }
              return { data: "oke" }
            }}
            canChange={canChange}
            showAlertDialog={() => setToggleConfirmDialog(true)}
            isUser={true}
          />
        </Route>
        <Route path={`/user-management/view/:code`}>
          {({ match }) => {
            return (
              <EntityCrudPage
                onModify={update}
                moduleName={moduleName}
                code={match?.params.code}
                get={GetById}
                formModel={viewForm}
                actions={viewActions}
                homePageUrl={HomePageURL.user}
                optionHeaderField="kanji"
                backHome={true}
                isUser={true}
              />
            )
          }}
        </Route>
        <Route path={`/user-management/:code`}>
          {({ match }) => {
            return (
              <EntityCrudPage
                setEditEntity={setEditEntity}
                onModify={update}
                moduleName={moduleName}
                code={match?.params.code}
                get={GetById}
                formModel={updateForm}
                actions={actions}
                homePageUrl={HomePageURL.user}
                optionHeaderField="kanji"
                submitSuccessMsg={"USER.UPDATE_USER.SUCCESS_MSG"}
                backDetail={"id"}
                backHome={true}
                onSecondModify={async (e) => {
                  if (onChange) {
                    // const result = isBanned ? await BanUser(e) : await UnbanUser(e);
                    // if (!result.error) setTrigger(!trigger)
                    setOnChange(false)
                    setRole("user")
                    // return result;
                  }
                  return { data: "oke" }
                }}
                canChange={canChange}
                showAlertDialog={() => setToggleConfirmDialog(true)}
                isUser={true}
              />
            )
          }}
        </Route>
        <Route path={`${HomePageURL.user}`} exact={true}>
          <MasterHeader
            title={headerTitle}
            onSearch={(value) => {
              setPaginationProps(DefaultPagination)
              let filterProps = {};
              if (value.role) {
                filterProps = { ...value, role: value.role.value }
              } else {
                filterProps = value;
              }
              setFilterProps(filterProps);
            }}
            searchModel={searchModel}
          />
          <div className="user-body">
            <MasterBody
              title='USER.MASTER.TABLE.TITLE'
              onCreate={onCreate}
              createTitle='USER.MASTER.TABLE.CREATE'
              optionTitle={Object.keys(filterProps).length > 0 ? (total > 0 ? `${total} ${intl.formatMessage({ id: "COMMON_SEARCH.RESULT.RESULT" })}` : intl.formatMessage({ id: "COMMON_SEARCH.RESULT.NO_RESULT" })) : ""}
              selectedEntities={selectedEntities}
              entities={entities}
              total={total}
              columns={columns as any}
              loading={loading}
              paginationParams={paginationProps}
              setPaginationParams={setPaginationProps}
              isShowId={false}
            />
          </div>
        </Route>
      </Switch>
      <DeleteEntityDialog
        entity={deleteEntity}
        onDelete={async () => {
          setLoading(true);
          const result = deleteEntity.isBanned ? await UnbanUser(deleteEntity) : await BanUser(deleteEntity);
          if (result.success) {
            setTrigger(!trigger);
          }
          setShowDelete(false);
          setLoading(false);
        }}
        isShow={showDelete}
        loading={loading}
        error={error}
        onHide={() => {
          setShowDelete(false);
        }}
        title={deleteEntity?.isBanned ? unlockDialogTitle : lockDialogTitle}
        confirmMessage={lockConfirmMessage}
        bodyTitle={lockDialogBodyTitle}
        deletingMessage={" "}
        deleteBtn={deleteEntity?.isBanned ? unlockBtn : lockBtn}
        cancelBtn={"COMMON.BTN_CANCEL"}
        deleteSuccessToast={deleteEntity?.isBanned ? "USER.UNLOCK_USER.SUCCESS_MSG" : "USER.LOCK_USER.SUCCESS_MSG"}
      />
      <DeleteEntityDialog
        entity={editEntity}
        onDelete={async () => {
          setToggleConfirmDialog(false);
          setCanChangeStatus(true);
          document.getElementById('save-user')?.click();
        }}
        isShow={showToggleConfirm}
        loading={loading}
        error={error}
        onHide={() => {
          setToggleConfirmDialog(false);
          setCanChangeStatus(false);
        }}
        confirmMessage={lockConfirmMessage}
        bodyTitle={lockDialogBodyTitle}
        deletingMessage={" "}
        // deleteBtn={isBanned ? unlockBtn : lockBtn}
        deleteBtn={unlockBtn}
        cancelBtn={"COMMON.BTN_CANCEL"}
        // title={isBanned ? unlockDialogTitle : lockDialogTitle}
        title={unlockDialogTitle}
      />
      <NotifyDialog
        title={"COMMON.AUTH.NOT_HAVE_PERMISSION.TITLE"}
        notifyMessage={" "}
        isShow={showPermissionDeny}
        onHide={() => setShowPermissionDeny(false)}
      />
    </Fragment>
  );
}

export default User
