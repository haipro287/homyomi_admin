import axios from 'axios';
import { API_BASE_URL } from '../../common-library/common-consts/enviroment';
import {
  CountProps,
  CreateProps,
  DeleteManyProps,
  DeleteProps,
  GetAllPropsServer,
  GetProps,
  UpdateProps,
} from '../../common-library/common-types/common-type';
import { formatParamsGet, formatTimeToUnix } from '../../common-library/helpers/format-params';

export const API_URL = API_BASE_URL + `/users`;

export const BULK_API_URL = API_URL + '/bulk'

export const Create: CreateProps<any> = (data: any) => {
  return axios.post(API_URL, data);
};

export const GetAll: GetAllPropsServer<any> = ({
  queryProps,
  sortList,
  paginationProps,
}) => {
  let params = formatParamsGet(queryProps, sortList, paginationProps);
  // params = params.to_date ? { ...params, to_date: Math.floor((params.to_date + 86399400)) } : params;
  return axios.get(`${API_URL}`, {
    params,
  });
};

export const GetById = (_id: string) => {
  return axios.get(`${API_URL}/${_id}`);
};

export const Update: UpdateProps<any> = (entity: any) => {
  // const birthday = entity?.birthday;

  // const newEntity = { ...entity, birthday: formatTimeToUnix(birthday) }

  return axios.put(`${API_URL}/${entity.id ?? entity.code}`, entity);
};

export const Count: CountProps<any> = (queryProps) => {
  return axios.get(`${API_URL}/get/count`, {
    params: { ...queryProps },
  });
};

export const Get: GetProps<any> = (entity) => {
  return axios.get(`${API_URL}/${entity._id}`);
};

export const Delete: DeleteProps<any> = (entity: any) => {
  return axios.delete(`${API_URL}/${entity._id}`);
};

export const DeleteMany: DeleteManyProps<any> = (entities: any[]) => {
  return axios.delete(BULK_API_URL, {
    data: { data: entities },
  });
};

export const BanUser: any = (entity: any) => {
  const id: any = entity?.id ?? entity.code;
  return axios.post(`${API_URL}/${id}/ban`, {
    id: id,
  });
};

export const UnbanUser: any = (entity: any) => {
  const id: any = entity?.id ?? entity.code;
  return axios.post(`${API_URL}/${id}/unban`, {
    id: id,
  });
};
