import React, { Fragment, useEffect, useMemo, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { DefaultPagination, HomePageURL, iconStyle, NormalColumn } from '../../common-library/common-consts/const';
import { MasterHeader } from '../../common-library/common-components/master-header';
import {
  ModifyForm,
  ModifyInputGroup,
  RenderInfoDetail,
  SearchModel
} from '../../common-library/common-types/common-type';
import { InitMasterProps } from '../../common-library/helpers/common-function';
import { Route, Switch, useHistory } from 'react-router-dom';
import { Count, Create, Delete, DeleteMany, Get, GetAll, GetById, Update } from "./admin.service";
import { MasterBody } from "../../common-library/common-components/master-body";
import { Tooltip } from '@material-ui/core';
import { ActionsColumnFormatter } from '../../common-library/common-components/actions-column-formatter';
import { DeleteEntityDialog } from '../../common-library/common-components/delete-entity-dialog';
import EntityCrudPage from '../../common-library/common-components/entity-crud-page';
import { Spinner } from 'react-bootstrap';
import SaveOutlinedIcon from '@material-ui/icons/SaveOutlined';
import CancelOutlinedIcon from '@material-ui/icons/CancelOutlined';
import { MasterEntityDetailDialog } from '../../common-library/common-components/master-entity-detail-dialog';
import { NotifyDialog } from '../../common-library/common-components/notify-dialog';
import { RootStateOrAny, useSelector } from 'react-redux';
import { Edit } from '@material-ui/icons';

const headerTitle = 'ADMIN.MASTER.HEADER.TITLE';

function Admin() {
  const intl = useIntl();

  const {
    entities,
    setEntities,
    deleteEntity,
    setDeleteEntity,
    createEntity,
    setCreateEntity,
    editEntity,
    setEditEntity,
    selectedEntities,
    setSelectedEntities,
    detailEntity,
    showDelete,
    setShowDelete,
    showDeleteMany,
    setShowDeleteMany,
    showDetail,
    setShowDetail,
    paginationProps,
    setPaginationProps,
    filterProps,
    setFilterProps,
    total,
    loading,
    setLoading,
    error,
    add,
    update,
    get,
    deleteMany,
    deleteFn,
    getAll,
    formatLongString,
  } = InitMasterProps<any>({
    getServer: Get,
    countServer: Count,
    createServer: Create,
    deleteServer: Delete,
    deleteManyServer: DeleteMany,
    getAllServer: GetAll,
    updateServer: Update,
  });

  const createTitle = 'ADMIN.MASTER.TABLE.CREATE';
  const updateTitle = 'ADMIN.MASTER.TABLE.TITLE';
  const viewTitle = 'ADMIN.MASTER.TABLE.TITLE';

  const [currentTab, setCurrentTab] = useState<string | undefined>('0');
  const [showPermissionDeny, setShowPermissionDeny] = useState<boolean>(false);
  const [trigger, setTrigger] = useState<boolean>(false);

  const history = useHistory();

  const userInfo = useSelector(({ auth }: { auth: RootStateOrAny }) => auth);

  useEffect(() => {
    getAll(filterProps);
  }, [paginationProps, filterProps, trigger, currentTab]);

  const columns = useMemo(() => {
    return [
      {
        dataField: 'mail',
        text: `${intl.formatMessage({ id: 'ADMIN.MASTER.TABLE.MAIL' })}`,
        headerClasses: 'text-center',
        formatter: (input: any) => <Tooltip children={<span>{formatLongString(input, 10, 10)}</span>} title={input} />,
        align: 'center',
      },
      {
        dataField: 'name',
        text: `${intl.formatMessage({ id: 'ADMIN.MASTER.TABLE.ADMIN_NAME' })}`,
        formatter: (input: any) => <Tooltip children={<span>{formatLongString(input, 6, 6)}</span>} title={input} />,
        headerClasses: 'text-center',
        align: 'center',
      },
      {
        dataField: 'role',
        text: `${intl.formatMessage({ id: 'ADMIN.MASTER.TABLE.TYPE' })}`,
        formatter: (input: any) => <FormattedMessage id={`ADMIN.MODIFY.TYPE_OPTION.${input.toString().replace('-', '').toUpperCase()}`} />,
        align: 'center',
        headerClasses: 'text-center',
      },
      {
        dataField: 'action',
        text: `${intl.formatMessage({ id: 'PROVIDER.MASTER.TABLE.ACTION_COLUMN' })}`,
        formatter: ActionsColumnFormatter,
        formatExtraData: {
          intl,
          onShowDetail: (entity: any) => {
            history.push(`${window.location.pathname}/view/${entity.id}`);
          },
          onEdit: (entity: any) => {
            if (userInfo.role === "super-admin") {
              setEditEntity(entity);
              history.push(`${window.location.pathname}/${entity.id}`);
            } else {
              setShowPermissionDeny(true);
            }
          },
          onDelete: (entity: any) => {
            if (userInfo.role === "super-admin") {
              setDeleteEntity(entity);
              setShowDelete(true);
            } else {
              setShowPermissionDeny(true);
            }
          },
        },
        ...NormalColumn,
        style: { minWidth: '130px' },
      }
    ]
  }, []);

  const searchModel: SearchModel = useMemo(() => ({
    mail: {
      type: 'string',
      label: 'ADMIN.MASTER.SEARCH.MAIL',
      placeholder: 'ADMIN.MASTER.SEARCH.MAIL'
    },
    name: {
      type: 'string',
      label: 'ADMIN.MASTER.SEARCH.NAME',
      placeholder: 'ADMIN.MASTER.SEARCH.NAME'
    },
    role: {
      type: 'search-select',
      onSearch: () => (
        {
          "data": [
            { label: intl.formatMessage({ id: "ADMIN.MODIFY.TYPE_OPTION.SUPERADMIN" }), value: "super-admin" },
            { label: intl.formatMessage({ id: "ADMIN.MODIFY.TYPE_OPTION.USERADMIN" }), value: "user-admin" },
            { label: intl.formatMessage({ id: "ADMIN.MODIFY.TYPE_OPTION.SERVICEADMIN" }), value: "service-admin" },
            { label: intl.formatMessage({ id: "ADMIN.MODIFY.TYPE_OPTION.STAFF" }), value: "staff" }
          ]
        }
      ),
      label: 'ADMIN.MASTER.SEARCH.TYPE',
      placeholder: 'ADMIN.MASTER.SEARCH.TYPE'
    },
  }), [currentTab]);

  const [group1, setGroup1] = useState<ModifyInputGroup>({
    _subTitle: '',
    mail: {
      _type: 'email',
      label: 'ADMIN.MASTER.TABLE.MAIL',
      required: true,
    },
    name: {
      _type: 'string-1',
      label: 'ADMIN.MASTER.TABLE.NAME',
      required: true,
    },
    jobTitle: {
      _type: 'string',
      label: 'ADMIN.MASTER.TABLE.JOB_TITLE',
    },
    role: {
      _type: 'radio',
      required: true,
      label: 'ADMIN.MASTER.TABLE.TYPE',
      options: [
        { label: intl.formatMessage({ id: 'ADMIN.MODIFY.TYPE_OPTION.USERADMIN' }), value: 'user-admin' },
        { label: intl.formatMessage({ id: 'ADMIN.MODIFY.TYPE_OPTION.SERVICEADMIN' }), value: 'service-admin' },
        { label: intl.formatMessage({ id: 'ADMIN.MODIFY.TYPE_OPTION.STAFF' }), value: 'staff' },
      ]
    },
  });

  const [editGroup1, setEditGroup1] = useState<ModifyInputGroup>({
    _subTitle: '',
    mail: {
      _type: 'email',
      label: 'ADMIN.MASTER.TABLE.MAIL',
      disabled: true,
    },
    name: {
      _type: 'string-1',
      label: 'ADMIN.MASTER.TABLE.NAME',
      required: true,
    },
    jobTitle: {
      _type: 'string',
      label: 'ADMIN.MASTER.TABLE.JOB_TITLE',
    },
    role: {
      _type: 'radio',
      required: true,
      label: 'ADMIN.MASTER.TABLE.TYPE',
      options: [
        { label: intl.formatMessage({ id: 'ADMIN.MODIFY.TYPE_OPTION.USERADMIN' }), value: 'user-admin' },
        { label: intl.formatMessage({ id: 'ADMIN.MODIFY.TYPE_OPTION.SERVICEADMIN' }), value: 'service-admin' },
        { label: intl.formatMessage({ id: 'ADMIN.MODIFY.TYPE_OPTION.STAFF' }), value: 'staff' },
      ]
    },
  });

  const [viewGroup1, setViewGroup1] = useState<ModifyInputGroup>({
    _subTitle: '',
    mail: {
      _type: 'email',
      label: 'ADMIN.MASTER.TABLE.MAIL',
      disabled: true,
    },
    name: {
      _type: 'string-1',
      disabled: true,
      label: 'ADMIN.MASTER.TABLE.NAME',
    },
    jobTitle: {
      _type: 'string',
      disabled: true,
      label: 'ADMIN.MASTER.TABLE.JOB_TITLE',
    },
    role: {
      _type: 'string-1',
      disabled: true,
      label: 'ADMIN.MASTER.TABLE.TYPE',
      formatter: (input) => {
        return input ? intl.formatMessage({ id: `ADMIN.MODIFY.TYPE_OPTION.${input?.toString()?.toUpperCase()}` }) : '';
      }
    },
  });

  const viewActions: any = useMemo(() => ({
    type: 'inside',
    data: {
      edit: {
        role: 'link-button',
        type: 'link',
        linkto: `/admin-management`,
        queryID: true,
        className: 'ml-8 btn btn-primary fixed-btn-width',
        label: 'COMMON.BTN_EDIT_USER',
        icon: <Edit />,
        guard: userInfo.role === "super-admin",
      },
    }
  }), [loading]);

  const createForm = useMemo((): ModifyForm => ({
    _header: createTitle,
    panel1: {
      _title: '',
      group1: group1,
    },
  }), [group1]);

  const updateForm = useMemo((): ModifyForm => ({
    _header: updateTitle,
    panel1: {
      _title: '',
      group1: editGroup1,
    },
  }), [group1]);

  const viewForm = useMemo((): ModifyForm => ({
    _header: viewTitle,
    panel1: {
      _title: '',
      group1: viewGroup1,
    },
  }), [group1]);

  const actions: any = useMemo(() => ({
    type: 'inside',
    data: {
      save: {
        role: 'submit',
        type: 'submit',
        linkto: undefined,
        className: 'btn btn-primary mr-8 fixed-btn-width',
        label: 'SAVE_BTN_LABEL',
        icon: loading ? (<Spinner style={iconStyle} animation="border" variant="light" size="sm" />) :
          (<SaveOutlinedIcon style={iconStyle} />)
      },
      cancel: {
        role: 'link-button',
        type: 'button',
        linkto: '/admin-management',
        className: 'btn btn-outline-primary fixed-btn-width',
        label: 'COMMON.BTN_CANCEL',
        icon: <CancelOutlinedIcon />,
      }
    }
  }), [loading]);

  const onCreate = () => {
    history.push(`${window.location.pathname}/0000000`);
  }

  return (
    <Fragment>
      <Switch>
        <Route path={`${HomePageURL.admin}/0000000`}>
          <EntityCrudPage
            mode={"vertical"}
            moduleName={"MODULE.NAME"}
            onModify={add}
            formModel={createForm}
            entity={createEntity}
            actions={actions}
            homePageUrl={HomePageURL.admin}
            submitSuccessMsg={"ADMIN.ADD_ADMIN.SUCCESS_MSG"}
            backDetail={"id"}
          />
        </Route>
        <Route path={`${HomePageURL.admin}/view/:code`}>
          {({ match }) => (
            <EntityCrudPage
              mode={"vertical"}
              onModify={update}
              setEditEntity={setEditEntity}
              moduleName={"MODULE.NAME"}
              code={match?.params.code}
              get={GetById}
              formModel={viewForm}
              homePageUrl={HomePageURL.admin}
              backHome={true}
              actions={viewActions}
            />
          )}
        </Route>
        <Route path={`${HomePageURL.admin}/:code`}>
          {({ match }) => (
            <EntityCrudPage
              mode={"vertical"}
              onModify={update}
              setEditEntity={setEditEntity}
              moduleName={"MODULE.NAME"}
              code={match?.params.code}
              get={GetById}
              formModel={updateForm}
              actions={actions}
              homePageUrl={HomePageURL.admin}
              submitSuccessMsg={"ADMIN.UPDATE_ADMIN.SUCCESS_MSG"}
              backDetail={"id"}
              backHome={true}
            />
          )}
        </Route>

        <Route path={`${HomePageURL.admin}`} exact={true}>
          <MasterHeader
            title={headerTitle}
            onSearch={(value) => {
              setPaginationProps(DefaultPagination)
              let filterProps = {};
              if (value.role) {
                filterProps = { ...value, role: value.role.value }
              } else {
                filterProps = value;
              }
              setFilterProps(filterProps);
            }}
            searchModel={searchModel}
          />
          <div className="activity-body">
            <MasterBody
              title='ADMIN.MASTER.TABLE.TITLE'
              optionTitle={Object.keys(filterProps).length > 0 ? (total > 0 ? `${total} ${intl.formatMessage({ id: "COMMON_SEARCH.RESULT.RESULT" })}` : intl.formatMessage({ id: "COMMON_SEARCH.RESULT.NO_RESULT" })) : ""}
              selectedEntities={selectedEntities}
              onCreate={onCreate}
              createTitle={createTitle}
              entities={entities}
              total={total}
              columns={columns as any}
              loading={loading}
              paginationParams={paginationProps}
              setPaginationParams={setPaginationProps}
              isShowId={false}
            />
          </div>
        </Route>
      </Switch>
      <DeleteEntityDialog
        entity={deleteEntity}
        onDelete={deleteFn}
        isShow={showDelete}
        loading={loading}
        error={error}
        onHide={() => {
          setShowDelete(false);
        }}
        title={"ADMIN.DELETE_ADMIN.TITLE"}
        confirmMessage={"ADMIN.DELETE_ADMIN_MSG"}
        bodyTitle={"ADMIN.DELETE_ADMIN.BODY_MSG"}
        deletingMessage={" "}
        deleteSuccessToast={"ADMIN.DELETE_ADMIN.SUCCESS_MSG"}
        deleteBtn={"ADMIN.DELETE_ADMIN.DELETE_BTN"}
        cancelBtn={"COMMON.BTN_CANCEL"}
      />
      <NotifyDialog
        title={"COMMON.AUTH.NOT_HAVE_PERMISSION.TITLE"}
        notifyMessage={" "}        isShow={showPermissionDeny}
        onHide={() => setShowPermissionDeny(false)}
      />
    </Fragment>
  );
}

export default Admin
