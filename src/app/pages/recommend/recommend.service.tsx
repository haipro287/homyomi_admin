import axios from 'axios';
import { API_BASE_URL } from '../../common-library/common-consts/enviroment';
import {
  CountProps,
  CreateProps,
  DeleteManyProps,
  DeleteProps,
  GetAllPropsServer,
  GetProps,
  UpdateProps,
} from '../../common-library/common-types/common-type';
import { formatParamsGet } from '../../common-library/helpers/format-params';
import firebase from "firebase/compat";

export const API_URL = API_BASE_URL + `/manga/recommended`;

export const BULK_API_URL = API_URL + '/bulk';

const upload = async (file: any) => {
}

export const Create: CreateProps<any> = async (data: any) => {
  const storage = firebase.storage();
  const ref = storage.ref();
  let url;
  const imgRef = ref.child("manga").child("recommended").child(data.mangaId).put(data.imageUrl.file)
  imgRef.on('state_changed', function (snapshot) {
  }, function (error) {
    console.log(error);
  }, async function () {
    // Uploaded completed successfully, now we can get the download URL

  });
  await imgRef;
  await imgRef.snapshot.ref.getDownloadURL().then(function (downloadURL) {
    //getting url of image
    url = downloadURL;
  });
  return axios.post(`${API_URL}`, { ...data, imageUrl: url });
  // return Promise.all([axios.post(`${API_URL}`, {...data, imageUrl: "hello"}), upload([])]);
};

export const GetAll: GetAllPropsServer<any> = ({
  queryProps,
  sortList,
  paginationProps,
}) => {
  const params = formatParamsGet(queryProps, sortList, paginationProps, true);
  return axios.get(`${API_URL}`, {
    params,
  });
};

export const Count: CountProps<any> = (queryProps) => {
  return axios.get(`${API_URL}/get/count`, {
    params: { ...queryProps },
  });
};

export const GetById = (_id: string) => {
  return axios.get(`${API_URL}/${_id}`);
};

export const Get: GetProps<any> = (entity) => {
  return axios.get(`${API_URL}/${entity.id}`);
};

export const Update: UpdateProps<any> = (entity: any) => {
  return axios.put(`${API_URL}/${entity.id}`, entity);
};

export const Delete: DeleteProps<any> = (entity: any) => {
  return axios.delete(`${API_URL}/${entity.id}`, {
    "data":
    {
      providerId: entity.providerId
    }
  });
};

export const DeleteMany: DeleteManyProps<any> = (entities: any[]) => {
  return axios.delete(BULK_API_URL, {
    data: { data: entities },
  });
};

