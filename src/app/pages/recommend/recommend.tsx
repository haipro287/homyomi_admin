import React, { Fragment, useEffect, useMemo, useState } from 'react';
import { useIntl } from 'react-intl';
import {
  dangerIconStyle,
  DefaultPagination,
  HomePageURL,
  iconStyle,
  NormalColumn,
  successIconStyle,
} from '../../common-library/common-consts/const';
import { MasterHeader } from '../../common-library/common-components/master-header';
import {
  ModifyForm,
  ModifyInputGroup,
  RenderInfoDetail,
  SearchModel
} from '../../common-library/common-types/common-type';
import { InitMasterProps, } from '../../common-library/helpers/common-function';
import { Route, Switch, useHistory } from 'react-router-dom';
import { Count, Create, Delete, DeleteMany, Get, GetAll, GetById, Update } from "./recommend.service";
import { MasterBody } from "../../common-library/common-components/master-body";
import { ActionsColumnFormatter } from '../../common-library/common-components/actions-column-formatter';
import { Spinner } from 'react-bootstrap';
import SaveOutlinedIcon from '@material-ui/icons/SaveOutlined';
import CancelOutlinedIcon from '@material-ui/icons/CancelOutlined';
import EntityCrudPage from '../../common-library/common-components/entity-crud-page';
import { MasterEntityDetailDialog } from '../../common-library/common-components/master-entity-detail-dialog';
import { DeleteEntityDialog } from '../../common-library/common-components/delete-entity-dialog';
import { RootStateOrAny, useSelector } from 'react-redux';
import { NotifyDialog } from '../../common-library/common-components/notify-dialog';
import { DisplayDateTime, DisplayImage } from '../../common-library/helpers/detail-helpers';
import { BlockOutlined, CheckOutlined, Edit } from '@material-ui/icons';
import { Tooltip } from '@material-ui/core';
import * as MangaService from "../manga/manga.service";

const headerTitle = 'RECOMMENDED.MASTER.HEADER.TITLE';

function Recommend() {
  const intl = useIntl();

  const {
    entities,
    setEntities,
    deleteEntity,
    setDeleteEntity,
    createEntity,
    setCreateEntity,
    selectedEntities,
    setSelectedEntities,
    detailEntity,
    editEntity,
    setEditEntity,
    showDelete,
    setShowDelete,
    showDeleteMany,
    setShowDeleteMany,
    showDetail,
    setShowDetail,
    paginationProps,
    setPaginationProps,
    filterProps,
    setFilterProps,
    total,
    loading,
    setLoading,
    error,
    add,
    update,
    get,
    deleteMany,
    deleteFn,
    getAll,
    convertUnixTimeToDate,
    formatLongString,
  } = InitMasterProps<any>({
    getServer: Get,
    countServer: Count,
    createServer: Create,
    deleteServer: Delete,
    deleteManyServer: DeleteMany,
    getAllServer: GetAll,
    updateServer: Update,
  });

  const createTitle = 'RECOMMENDED.MASTER.TABLE.CREATE';
  const updateTitle = 'RECOMMENDED.MASTER.TABLE.UPDATE';
  const viewTitle = 'RECOMMENDED.MASTER.TABLE.TITLE';

  const [currentTab, setCurrentTab] = useState<string | undefined>('0');
  const [trigger, setTrigger] = useState<boolean>(false);
  const [isNsfw, setIsNsfw] = useState<boolean>(false);
  const [canChange, setCanChangeStatus] = useState<boolean>(true);
  const [onChange, setOnChange] = useState<boolean>(false);

  const history = useHistory();
  const userInfo = useSelector(({ auth }: { auth: RootStateOrAny }) => auth);

  const [showPermissionDeny, setShowPermissionDeny] = useState<boolean>(false);

  useEffect(() => {
    getAll(filterProps);
  }, [paginationProps, filterProps, trigger, currentTab]);

  const columns = useMemo(() => {
    return [
      {
        dataField: 'id',
        text: `${intl.formatMessage({ id: 'RECOMMENDED.MASTER.TABLE.RECOMMENDED_ID' })}`,
        headerClasses: 'text-center',
        formatter: (input: any) => <Tooltip children={<span>{formatLongString(input, 10, 10)}</span>} title={input}/>,
        align: 'center',
      },
      {
        dataField: 'manga',
        text: `${intl.formatMessage({ id: 'RECOMMENDED.MASTER.TABLE.TITLE' })}`,
        headerClasses: 'text-center',
        formatter: (input: any) => input.title,
        align: 'center',
      },
      {
        dataField: 'titleColor',
        text: `${intl.formatMessage({ id: 'RECOMMENDED.MASTER.TABLE.TITLE_COLOR' })}`,
        headerClasses: 'text-center',
        align: 'center',
      },
      {
        dataField: 'description',
        text: `${intl.formatMessage({ id: 'RECOMMENDED.MASTER.TABLE.RECOMMENDED_DESCRIPTION' })}`,
        headerClasses: 'text-center',
        formatter: (input: any) => <Tooltip children={<span>{formatLongString(input, 10, 10)}</span>} title={input}/>,
        align: 'center',
      },
      {
        dataField: 'imageUrl',
        class: "btn-primary",
        text: `${intl.formatMessage({ id: 'RECOMMENDED.MASTER.TABLE.IMAGE_URL' })}`,
        headerClasses: 'text-center',
        classes: 'text-center',
        formatter: DisplayImage
      },
      {
        dataField: 'isPublished',
        text: `${intl.formatMessage({ id: 'RECOMMENDED.MASTER.TABLE.ISPUBLISHED' })}`,
        // formatter: (input: any) => <FormattedDate value={convertUnixTimeToDate(input)} />,
        headerClasses: 'text-center',
        align: 'center',
      },
      {
        dataField: 'createdAt',
        text: `${intl.formatMessage({ id: 'USER.ACCOUNT_INFO.JOIN_DATE' })}`,
        // formatter: (input: any) => <FormattedDate value={convertUnixTimeToDate(input)} />,
        headerClasses: 'text-center',
        align: 'center',
      },
      {
        dataField: 'action',
        text: `${intl.formatMessage({ id: 'RECOMMENDED.MASTER.TABLE.ACTION_COLUMN' })}`,
        formatter: ActionsColumnFormatter,
        formatExtraData: {
          intl,
          onShowDetail: (entity: any) => {
            history.push(`${window.location.pathname}/view/${entity.id}`);
            // get(entity);
            // setShowDetail(true);
          },
          onEdit: (entity: any) => {
            if (true) {
              // if (["service-admin", "super-admin"].includes(userInfo.role)) {
              setEditEntity(entity);
              history.push(`${window.location.pathname}/${entity.id}`);
            } else {
              setShowPermissionDeny(true);
            }
          },
          onDelete: (entity: any) => {
            if (true) {
              // if (["service-admin", "super-admin"].includes(userInfo.role)) {
              setDeleteEntity(entity);
              setShowDelete(true);
            } else {
              setShowPermissionDeny(true);
            }
          },
        },
        ...NormalColumn,
        style: { minWidth: '130px' },
      }
    ];
  }, []);

  const searchModel: SearchModel = useMemo(() => ({
    // providerId: {
    //   type: 'string',
    //   label: 'RECOMMENDED.MASTER.SEARCH.RECOMMENDED_ID',
    //   placeholder: 'RECOMMENDED.MASTER.SEARCH.RECOMMENDED_ID'
    // },
    title: {
      type: 'string',
      label: 'RECOMMENDED.MASTER.SEARCH.RECOMMENDED_TITLE',
      placeholder: 'RECOMMENDED.MASTER.SEARCH.RECOMMENDED_TITLE'
    },
    // mail: {
    //   type: 'string',
    //   label: 'RECOMMENDED.MASTER.SEARCH.MAIL',
    //   placeholder: 'RECOMMENDED.MASTER.SEARCH.MAIL'
    // },
  }), [currentTab]);

  const [group1, setGroup1] = useState<ModifyInputGroup>({
    _subTitle: '',
    // providerId: {
    //   _type: 'string-alpha',
    //   label: 'RECOMMENDED.MASTER.TABLE.RECOMMENDED_ID',
    //   required: true,
    // },
    mangaId: {
      _type: 'search-select',
      label: 'RECOMMENDED.MASTER.TABLE.MANGA',
      onSearch: MangaService.GetAll,
      keyField: 'title',
      required: true,
      onChange: (input: any) => input.id,
    },
    titleColor: {
      _type: 'color',
      required: true,
      label: 'RECOMMENDED.MASTER.TABLE.TITLE_COLOR',
    },
    // manager: {
    //   _type: 'string',
    //   label: 'RECOMMENDED.MASTER.TABLE.MANAGER',
    // },
    // mail: {
    //   _type: 'string',
    //   label: 'RECOMMENDED.MASTER.TABLE.RECOMMENDED_DESCRIPTION',
    // },
    // phone: {
    //   _type: 'phone-number',
    //   label: 'RECOMMENDED.MASTER.TABLE.PHONE',
    //   required: true,
    // },
    description: {
      _type: 'text-field',
      label: 'RECOMMENDED.MASTER.TABLE.DESCRIPTION',
      required: true,
    },
    imageUrl: {
      // _type: 'string',
      _type: 'image',
    label: 'RECOMMENDED.MASTER.TABLE.IMAGE_URL',
      isArray: false,
      maxNumber: 1,
     /* onChange: (value) => {
        console.log(value.file[0]);
        return value;
      }*/
    },
    isPublished: {
      _type: 'radio',
      label: 'RECOMMENDED.MASTER.TABLE.ISPUBLISHED',
      options: [
        { label: intl.formatMessage({ id: 'RECOMMENDED.ISPUBLISHED.FALSE' }), value: false },
        { label: intl.formatMessage({ id: 'RECOMMENDED.ISPUBLISHED.TRUE' }), value: true },
      ],
      required: true,
    },
  });

  const [editGroup1, setEditGroup1] = useState<ModifyInputGroup>({
    _subTitle: '',
    // joinDate: {
    //   _type: 'date-time',
    //   label: 'RECOMMENDED.MASTER.TABLE.JOIN_DATE',
    //   disabled: true,
    // },
    id: {
      _type: 'string',
      label: 'RECOMMENDED.MASTER.TABLE.RECOMMENDED_ID',
      // formatter: (input: any) => {
      //   if (input) {
      //     return input.replace(/[^A-Za-z]/gi, '');
      //   }
      // },
      required: true,
      disabled: true,
    },
    mangaId: {
      _type: 'search-select',
      label: 'RECOMMENDED.MASTER.TABLE.MANGA',
      onSearch: MangaService.GetAll,
      keyField: 'title',
      required: true,
      onChange: (input: any) => input.id,
    },
    titleColor: {
      _type: 'color',
      required: true,
      label: 'RECOMMENDED.MASTER.TABLE.TITLE_COLOR',
    },
    // manager: {
    //   _type: 'string',
    //   label: 'RECOMMENDED.MASTER.TABLE.MANAGER',
    // },
    // mail: {
    //   _type: 'string',
    //   label: 'RECOMMENDED.MASTER.TABLE.RECOMMENDED_DESCRIPTION',
    // },
    // phone: {
    //   _type: 'phone-number',
    //   label: 'RECOMMENDED.MASTER.TABLE.PHONE',
    //   required: true,
    // },
    description: {
      _type: 'text-field',
      label: 'RECOMMENDED.MASTER.TABLE.DESCRIPTION',
      required: true,
    },
    imageUrl: {
      // _type: 'string',
      _type: 'image',
      label: 'RECOMMENDED.MASTER.TABLE.IMAGE_URL',
      isArray: false,
      maxNumber: 1,
      /* onChange: (value) => {
         console.log(value.file[0]);
         return value;
       }*/
    },
    isPublished: {
      _type: 'search-select',
      required: true,
      label: 'RECOMMENDED.MASTER.TABLE.ISPUBLISHED',
      onSearch: () => (
        {
          "data": [
            { label: intl.formatMessage({ id: 'RECOMMENDED.OPTION.FALSE' }), value: false },
            { label: intl.formatMessage({ id: 'RECOMMENDED.OPTION.TRUE' }), value: true },
          ]
        }
      ),
      onChange: (value) => {
        setIsNsfw(value?.value || false);
        setCanChangeStatus(!canChange);
        setOnChange(!onChange);
        return value.value;
      }
    },
  });

  const [viewGroup1, setViewGroup1] = useState<ModifyInputGroup>({
    _subTitle: '',
    // joinDate: {
    //   _type: 'date-time',
    //   label: 'RECOMMENDED.MASTER.TABLE.JOIN_DATE',
    //   disabled: true,
    // },
    id: {
      _type: 'string',
      label: 'RECOMMENDED.MASTER.TABLE.RECOMMENDED_ID',
      // formatter: (input: any) => {
      //   if (input) {
      //     return input.replace(/[^A-Za-z]/gi, '');
      //   }
      // },
      required: true,
      disabled: true,
    },
    title: {
      _type: 'string',
      label: 'RECOMMENDED.MASTER.TABLE.RECOMMENDED_NAME',
      required: true,
      disabled: true,
    },
    nsfw: {
      // _type: 'string',
      // label: 'USER.ACCOUNT_INFO.STATUS',
      // disabled: true,
      // formatter: (input: any) => {
      //   return input ? intl.formatMessage({ id: 'USER.OPTION.BANNED' }) : intl.formatMessage({ id: 'USER.OPTION.ACTIVE' })
      // }

      _type: 'search-select',
      label: 'USER.MASTER.SEARCH.ISBANNED',
      disabled: true,
      onSearch: () => (
        {
          "data": [
            { label: intl.formatMessage({ id: 'USER.OPTION.FALSE' }), value: false },
            { label: intl.formatMessage({ id: 'USER.OPTION.TRUE' }), value: true },
          ]
        }
      ),
    },
    // manager: {
    //   _type: 'string',
    //   disabled: true,
    //   label: 'RECOMMENDED.MASTER.TABLE.MANAGER',
    // },
    // mail: {
    //   _type: 'string',
    //   disabled: true,
    //   label: 'RECOMMENDED.MASTER.TABLE.RECOMMENDED_DESCRIPTION',
    // },
    // phone: {
    //   _type: 'string',
    //   disabled: true,
    //   label: 'RECOMMENDED.MASTER.TABLE.PHONE',
    // },
    description: {
      _type: 'text-field',
      disabled: true,
      label: 'RECOMMENDED.MASTER.TABLE.DESCRIPTION',
    },
  });

  const viewActions: any = useMemo(() => ({
    type: 'inside',
    data: {
      edit: {
        role: 'link-button',
        type: 'link',
        linkto: `/category-management`,
        queryID: true,
        className: 'ml-8 btn btn-primary fixed-btn-width',
        label: 'COMMON.BTN_EDIT_USER',
        icon: <Edit/>,
        // guard: ["service-admin", "super-admin"].includes(userInfo.role),
      },
    }
  }), [loading]);

  const createForm = useMemo((): ModifyForm => ({
    _header: createTitle,
    panel1: {
      _title: '',
      group1: group1,
    },
  }), [group1]);

  const updateForm = useMemo((): ModifyForm => ({
    _header: updateTitle,
    panel1: {
      _title: '',
      group1: editGroup1,
    },
  }), [editGroup1]);

  const viewForm = useMemo((): ModifyForm => ({
    _header: viewTitle,
    panel1: {
      _title: '',
      group1: viewGroup1,
    },
  }), [viewGroup1]);

  const actions: any = useMemo(() => ({
    type: 'inside',
    data: {
      save: {
        role: 'submit',
        type: 'submit',
        linkto: undefined,
        className: 'btn btn-primary mr-8 fixed-btn-width',
        label: 'SAVE_BTN_LABEL',
        icon: loading ? (<Spinner style={iconStyle} animation="border" variant="light" size="sm"/>) :
          (<SaveOutlinedIcon style={iconStyle}/>)
      },
      cancel: {
        role: 'link-button',
        type: 'button',
        linkto: '/category-management',
        className: 'btn btn-outline-primary fixed-btn-width',
        label: 'COMMON.BTN_CANCEL',
        icon: <CancelOutlinedIcon/>,
      }
    }
  }), [loading]);

  const onCreate = () => {
    history.push(`${window.location.pathname}/0000000`);
  };

  return (
    <Fragment>
      <Switch>
        <Route path={`${HomePageURL.recommended}/0000000`}>
          <EntityCrudPage
            mode={"vertical"}
            moduleName={"MODULE.NAME"}
            onModify={add}
            formModel={createForm}
            entity={createEntity}
            actions={actions}
            homePageUrl={HomePageURL.recommended}
            submitSuccessMsg={"RECOMMENDED.ADD_RECOMMENDED.SUCCESS_MSG"}
            backDetail={"providerId"}
          />
        </Route>
        <Route path={`${HomePageURL.recommended}/view/:code`}>
          {({ match }) => (
            <EntityCrudPage
              mode={"vertical"}
              onModify={update}
              setEditEntity={setEditEntity}
              moduleName={"MODULE.NAME"}
              code={match?.params.code}
              get={GetById}
              formModel={viewForm}
              homePageUrl={HomePageURL.recommended}
              backHome={true}
              actions={viewActions}
            />
          )}
        </Route>
        <Route path={`${HomePageURL.recommended}/:code`}>
          {({ match }) => (
            <EntityCrudPage
              mode={"vertical"}
              onModify={update}
              setEditEntity={setEditEntity}
              moduleName={"MODULE.NAME"}
              code={match?.params.code}
              get={GetById}
              formModel={updateForm}
              actions={actions}
              homePageUrl={HomePageURL.recommended}
              submitSuccessMsg={"RECOMMENDED.UPDATE_RECOMMENDED.SUCCESS_MSG"}
              backDetail={"providerId"}
              backHome={true}
            />
          )}
        </Route>
        <Route path={`${HomePageURL.recommended}`} exact={true}>
          <MasterHeader
            title={headerTitle}
            onSearch={(value) => {
              setPaginationProps(DefaultPagination);
              setFilterProps(value);
            }}
            searchModel={searchModel}
          />
          <div className="provider-body">
            <MasterBody
              // onCreate={["service-admin", "super-admin"].includes(userInfo.role) ? onCreate : undefined}
              onCreate={onCreate}
              createTitle="RECOMMENDED.MASTER.TABLE.CREATE"
              optionTitle={Object.keys(filterProps).length > 0 ? (total > 0 ? `${total} ${intl.formatMessage({ id: "COMMON_SEARCH.RESULT.RESULT" })}` : intl.formatMessage({ id: "COMMON_SEARCH.RESULT.NO_RESULT" })) : ""}
              title="RECOMMENDED.MASTER.TABLE.TITLE"
              selectedEntities={selectedEntities}
              entities={entities}
              total={total}
              columns={columns as any}
              loading={loading}
              paginationParams={paginationProps}
              setPaginationParams={setPaginationProps}
              isShowId={false}
            />
          </div>
        </Route>
      </Switch>
      <DeleteEntityDialog
        entity={deleteEntity}
        onDelete={deleteFn}
        deleteSuccessToast={"RECOMMENDED.DELETE.DELETE_SUCCESS"}
        isShow={showDelete}
        loading={loading}
        error={error}
        onHide={() => {
          setShowDelete(false);
        }}
        title={"RECOMMENDED.DELETE.TITLE"}
        confirmMessage={"RECOMMENDED.DELETE.MSG"}
        bodyTitle={"RECOMMENDED.DELETE.BODY_MSG"}
        deletingMessage={" "}
        cancelBtn={"COMMON.BTN_CANCEL"}
        deleteBtn={"RECOMMENDED.DELETE.DELETE_BTN"}
      />
      <NotifyDialog
        title={"COMMON.AUTH.NOT_HAVE_PERMISSION.TITLE"}
        notifyMessage={" "} isShow={showPermissionDeny}
        onHide={() => setShowPermissionDeny(false)}
      />
    </Fragment>
  );
}

export default Recommend;
