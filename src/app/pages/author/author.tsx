import React, { Fragment, useEffect, useMemo, useState } from 'react';
import { useIntl } from 'react-intl';
import { DefaultPagination, HomePageURL, iconStyle, NormalColumn, } from '../../common-library/common-consts/const';
import { MasterHeader } from '../../common-library/common-components/master-header';
import {
  ModifyForm,
  ModifyInputGroup,
  RenderInfoDetail,
  SearchModel
} from '../../common-library/common-types/common-type';
import { InitMasterProps, } from '../../common-library/helpers/common-function';
import { Route, Switch, useHistory } from 'react-router-dom';
import { Count, Create, Delete, DeleteMany, Get, GetAll, GetById, Update } from "./author.service";
import { MasterBody } from "../../common-library/common-components/master-body";
import { ActionsColumnFormatter } from '../../common-library/common-components/actions-column-formatter';
import { Spinner } from 'react-bootstrap';
import SaveOutlinedIcon from '@material-ui/icons/SaveOutlined';
import CancelOutlinedIcon from '@material-ui/icons/CancelOutlined';
import EntityCrudPage from '../../common-library/common-components/entity-crud-page';
import { MasterEntityDetailDialog } from '../../common-library/common-components/master-entity-detail-dialog';
import { DeleteEntityDialog } from '../../common-library/common-components/delete-entity-dialog';
import { RootStateOrAny, useSelector } from 'react-redux';
import { NotifyDialog } from '../../common-library/common-components/notify-dialog';
import { DisplayDateTime } from '../../common-library/helpers/detail-helpers';
import { Edit } from '@material-ui/icons';
import { Tooltip } from '@material-ui/core';

const headerTitle = 'AUTHOR.MASTER.HEADER.TITLE';

function Author() {
  const intl = useIntl();

  const {
    entities,
    setEntities,
    deleteEntity,
    setDeleteEntity,
    createEntity,
    setCreateEntity,
    selectedEntities,
    setSelectedEntities,
    detailEntity,
    editEntity,
    setEditEntity,
    showDelete,
    setShowDelete,
    showDeleteMany,
    setShowDeleteMany,
    showDetail,
    setShowDetail,
    paginationProps,
    setPaginationProps,
    filterProps,
    setFilterProps,
    total,
    loading,
    setLoading,
    error,
    add,
    update,
    get,
    deleteMany,
    deleteFn,
    getAll,
    convertUnixTimeToDate,
    formatLongString,
  } = InitMasterProps<any>({
    getServer: Get,
    countServer: Count,
    createServer: Create,
    deleteServer: Delete,
    deleteManyServer: DeleteMany,
    getAllServer: GetAll,
    updateServer: Update,
  });

  const createTitle = 'AUTHOR.MASTER.TABLE.CREATE';
  const updateTitle = 'AUTHOR.MASTER.TABLE.UPDATE';
  const viewTitle = 'AUTHOR.MASTER.TABLE.TITLE';

  const [currentTab, setCurrentTab] = useState<string | undefined>('0');
  const [trigger, setTrigger] = useState<boolean>(false);

  const history = useHistory();
  const userInfo = useSelector(({ auth }: { auth: RootStateOrAny }) => auth);

  const [showPermissionDeny, setShowPermissionDeny] = useState<boolean>(false);

  useEffect(() => {
    getAll(filterProps);
  }, [paginationProps, filterProps, trigger, currentTab]);

  const columns = useMemo(() => {
    return [
      {
        dataField: 'id',
        text: `${intl.formatMessage({ id: 'AUTHOR.MASTER.TABLE.AUTHOR_ID' })}`,
        headerClasses: 'text-center',
        formatter: (input: any) => <Tooltip children={<span>{formatLongString(input, 10, 10)}</span>} title={input} />,
        align: 'center',
      },
      {
        dataField: 'name',
        text: `${intl.formatMessage({ id: 'AUTHOR.MASTER.TABLE.AUTHOR_NAME' })}`,
        headerClasses: 'text-center',
        formatter: (input: any) => <Tooltip children={<span>{formatLongString(input, 10, 10)}</span>} title={input} />,
        align: 'center',
      },
      {
        dataField: 'description',
        text: `${intl.formatMessage({ id: 'AUTHOR.MASTER.TABLE.AUTHOR_MAIL' })}`,
        headerClasses: 'text-center',
        formatter: (input: any) => <Tooltip children={<span>{formatLongString(input, 10, 10)}</span>} title={input} />,
        align: 'center',
      },
      {
        dataField: 'action',
        text: `${intl.formatMessage({ id: 'AUTHOR.MASTER.TABLE.ACTION_COLUMN' })}`,
        formatter: ActionsColumnFormatter,
        formatExtraData: {
          intl,
          onShowDetail: (entity: any) => {
            history.push(`${window.location.pathname}/view/${entity.id}`);
            // get(entity);
            // setShowDetail(true);
          },
          onEdit: (entity: any) => {
            if (true) {
            // if (["service-admin", "super-admin"].includes(userInfo.role)) {
              setEditEntity(entity);
              history.push(`${window.location.pathname}/${entity.id}`);
            } else {
              setShowPermissionDeny(true)
            }
          },
          onDelete: (entity: any) => {
            if (true) {
            // if (["service-admin", "super-admin"].includes(userInfo.role)) {
              setDeleteEntity(entity);
              setShowDelete(true);
            } else {
              setShowPermissionDeny(true)
            }
          },
        },
        ...NormalColumn,
        style: { minWidth: '130px' },
      }
    ]
  }, []);

  const searchModel: SearchModel = useMemo(() => ({
    // providerId: {
    //   type: 'string',
    //   label: 'AUTHOR.MASTER.SEARCH.AUTHOR_ID',
    //   placeholder: 'AUTHOR.MASTER.SEARCH.AUTHOR_ID'
    // },
    name: {
      type: 'string',
      label: 'AUTHOR.MASTER.SEARCH.AUTHOR_NAME',
      placeholder: 'AUTHOR.MASTER.SEARCH.AUTHOR_NAME'
    },
    // mail: {
    //   type: 'string',
    //   label: 'AUTHOR.MASTER.SEARCH.MAIL',
    //   placeholder: 'AUTHOR.MASTER.SEARCH.MAIL'
    // },
  }), [currentTab]);

  const [group1, setGroup1] = useState<ModifyInputGroup>({
    _subTitle: '',
    // providerId: {
    //   _type: 'string-alpha',
    //   label: 'AUTHOR.MASTER.TABLE.AUTHOR_ID',
    //   required: true,
    // },
    name: {
      _type: 'string',
      label: 'AUTHOR.MASTER.TABLE.AUTHOR_NAME',
      required: true,
    },
    // manager: {
    //   _type: 'string',
    //   label: 'AUTHOR.MASTER.TABLE.MANAGER',
    // },
    // mail: {
    //   _type: 'string',
    //   label: 'AUTHOR.MASTER.TABLE.AUTHOR_MAIL',
    // },
    // phone: {
    //   _type: 'phone-number',
    //   label: 'AUTHOR.MASTER.TABLE.PHONE',
    //   required: true,
    // },
    description: {
      _type: 'text-field',
      label: 'AUTHOR.MASTER.TABLE.DESCRIPTION',
    },
  });

  const [editGroup1, setEditGroup1] = useState<ModifyInputGroup>({
    _subTitle: '',
    // joinDate: {
    //   _type: 'date-time',
    //   label: 'AUTHOR.MASTER.TABLE.JOIN_DATE',
    //   disabled: true,
    // },
    id: {
      _type: 'string',
      label: 'AUTHOR.MASTER.TABLE.AUTHOR_ID',
      // formatter: (input: any) => {
      //   if (input) {
      //     return input.replace(/[^A-Za-z]/gi, '');
      //   }
      // },
      required: true,
      disabled: true,
    },
    name: {
      _type: 'string',
      label: 'AUTHOR.MASTER.TABLE.AUTHOR_NAME',
      required: true,
    },
    // manager: {
    //   _type: 'string',
    //   label: 'AUTHOR.MASTER.TABLE.MANAGER',
    // },
    // mail: {
    //   _type: 'string',
    //   label: 'AUTHOR.MASTER.TABLE.AUTHOR_MAIL',
    // },
    // phone: {
    //   _type: 'phone-number',
    //   label: 'AUTHOR.MASTER.TABLE.PHONE',
    //   required: true,
    // },
    description: {
      _type: 'text-field',
      label: 'AUTHOR.MASTER.TABLE.DESCRIPTION',
    },
  });

  const [viewGroup1, setViewGroup1] = useState<ModifyInputGroup>({
    _subTitle: '',
    // joinDate: {
    //   _type: 'date-time',
    //   label: 'AUTHOR.MASTER.TABLE.JOIN_DATE',
    //   disabled: true,
    // },
    id: {
      _type: 'string',
      label: 'AUTHOR.MASTER.TABLE.AUTHOR_ID',
      // formatter: (input: any) => {
      //   if (input) {
      //     return input.replace(/[^A-Za-z]/gi, '');
      //   }
      // },
      required: true,
      disabled: true,
    },
    name: {
      _type: 'string',
      label: 'AUTHOR.MASTER.TABLE.AUTHOR_NAME',
      disabled: true,
      required: true,
    },
    // manager: {
    //   _type: 'string',
    //   disabled: true,
    //   label: 'AUTHOR.MASTER.TABLE.MANAGER',
    // },
    // mail: {
    //   _type: 'string',
    //   disabled: true,
    //   label: 'AUTHOR.MASTER.TABLE.AUTHOR_MAIL',
    // },
    // phone: {
    //   _type: 'string',
    //   disabled: true,
    //   label: 'AUTHOR.MASTER.TABLE.PHONE',
    // },
    description: {
      _type: 'text-field',
      disabled: true,
      label: 'AUTHOR.MASTER.TABLE.DESCRIPTION',
    },
  });

  const viewActions: any = useMemo(() => ({
    type: 'inside',
    data: {
      edit: {
        role: 'link-button',
        type: 'link',
        linkto: `/author-management`,
        queryID: true,
        className: 'ml-8 btn btn-primary fixed-btn-width',
        label: 'COMMON.BTN_EDIT_USER',
        icon: <Edit />,
        // guard: ["service-admin", "super-admin"].includes(userInfo.role),
      },
    }
  }), [loading]);

  const createForm = useMemo((): ModifyForm => ({
    _header: createTitle,
    panel1: {
      _title: '',
      group1: group1,
    },
  }), [group1]);

  const updateForm = useMemo((): ModifyForm => ({
    _header: updateTitle,
    panel1: {
      _title: '',
      group1: editGroup1,
    },
  }), [editGroup1]);

  const viewForm = useMemo((): ModifyForm => ({
    _header: viewTitle,
    panel1: {
      _title: '',
      group1: viewGroup1,
    },
  }), [viewGroup1]);

  const actions: any = useMemo(() => ({
    type: 'inside',
    data: {
      save: {
        role: 'submit',
        type: 'submit',
        linkto: undefined,
        className: 'btn btn-primary mr-8 fixed-btn-width',
        label: 'SAVE_BTN_LABEL',
        icon: loading ? (<Spinner style={iconStyle} animation="border" variant="light" size="sm" />) :
          (<SaveOutlinedIcon style={iconStyle} />)
      },
      cancel: {
        role: 'link-button',
        type: 'button',
        linkto: '/author-management',
        className: 'btn btn-outline-primary fixed-btn-width',
        label: 'COMMON.BTN_CANCEL',
        icon: <CancelOutlinedIcon />,
      }
    }
  }), [loading]);

  const onCreate = () => {
    history.push(`${window.location.pathname}/0000000`);
  }

  return (
    <Fragment>
      <Switch>
        <Route path={`${HomePageURL.author}/0000000`}>
          <EntityCrudPage
            mode={"vertical"}
            moduleName={"MODULE.NAME"}
            onModify={add}
            formModel={createForm}
            entity={createEntity}
            actions={actions}
            homePageUrl={HomePageURL.author}
            submitSuccessMsg={"AUTHOR.ADD_AUTHOR.SUCCESS_MSG"}
            backDetail={"providerId"}
          />
        </Route>
        <Route path={`${HomePageURL.author}/view/:code`}>
          {({ match }) => (
            <EntityCrudPage
              mode={"vertical"}
              onModify={update}
              setEditEntity={setEditEntity}
              moduleName={"MODULE.NAME"}
              code={match?.params.code}
              get={GetById}
              formModel={viewForm}
              homePageUrl={HomePageURL.author}
              backHome={true}
              actions={viewActions}
            />
          )}
        </Route>
        <Route path={`${HomePageURL.author}/:code`}>
          {({ match }) => (
            <EntityCrudPage
              mode={"vertical"}
              onModify={update}
              setEditEntity={setEditEntity}
              moduleName={"MODULE.NAME"}
              code={match?.params.code}
              get={GetById}
              formModel={updateForm}
              actions={actions}
              homePageUrl={HomePageURL.author}
              submitSuccessMsg={"AUTHOR.UPDATE_AUTHOR.SUCCESS_MSG"}
              backDetail={"providerId"}
              backHome={true}
            />
          )}
        </Route>
        <Route path={`${HomePageURL.author}`} exact={true}>
          <MasterHeader
            title={headerTitle}
            onSearch={(value) => {
              setPaginationProps(DefaultPagination)
              setFilterProps(value)
            }}
            searchModel={searchModel}
          />
          <div className="provider-body">
            <MasterBody
              // onCreate={["service-admin", "super-admin"].includes(userInfo.role) ? onCreate : undefined}
              onCreate={onCreate}
              createTitle='AUTHOR.MASTER.TABLE.CREATE'
              optionTitle={Object.keys(filterProps).length > 0 ? (total > 0 ? `${total} ${intl.formatMessage({ id: "COMMON_SEARCH.RESULT.RESULT" })}` : intl.formatMessage({ id: "COMMON_SEARCH.RESULT.NO_RESULT" })) : ""}
              title='AUTHOR.MASTER.TABLE.TITLE'
              selectedEntities={selectedEntities}
              entities={entities}
              total={total}
              columns={columns as any}
              loading={loading}
              paginationParams={paginationProps}
              setPaginationParams={setPaginationProps}
              isShowId={false}
            />
          </div>
        </Route>
      </Switch>
      <DeleteEntityDialog
        entity={deleteEntity}
        onDelete={deleteFn}
        deleteSuccessToast={"AUTHOR.DELETE.DELETE_SUCCESS"}
        isShow={showDelete}
        loading={loading}
        error={error}
        onHide={() => {
          setShowDelete(false);
        }}
        title={"AUTHOR.DELETE.TITLE"}
        confirmMessage={"AUTHOR.DELETE.MSG"}
        bodyTitle={"AUTHOR.DELETE.BODY_MSG"}
        deletingMessage={" "}
        cancelBtn={"COMMON.BTN_CANCEL"}
        deleteBtn={"AUTHOR.DELETE.DELETE_BTN"}
      />
      <NotifyDialog
        title={"COMMON.AUTH.NOT_HAVE_PERMISSION.TITLE"}
        notifyMessage={" "} isShow={showPermissionDeny}
        onHide={() => setShowPermissionDeny(false)}
      />
    </Fragment>
  );
}

export default Author;
