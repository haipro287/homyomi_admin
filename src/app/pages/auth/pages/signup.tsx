import React, { useCallback, useState } from 'react';
import { useFormik } from 'formik';
import { connect, RootStateOrAny, useSelector } from 'react-redux';
import * as Yup from 'yup';
import { useHistory } from 'react-router-dom';
import { FormattedMessage, injectIntl } from 'react-intl';
import * as auth from '../_redux/auth-redux';
import { Signup } from '../_redux/auth.service';
import { GenerateKeyPair, SignMessage, SymmetricDecrypt, SymmetricEncrypt } from '../service/auth-cryptography';
import { CERTIFICATE_EXP } from '../../../common-library/common-consts/enviroment';
import clsx from 'clsx';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {
  createMuiTheme,
  FormControl,
  IconButton,
  InputAdornment,
  InputLabel,
  makeStyles,
  MuiThemeProvider,
  OutlinedInput,
  TextField,
  Tooltip
} from "@material-ui/core";
import _ from 'lodash';
import QueryString from 'qs';

const initialValues = {
  newPassword: '',
  repeatNewPassword: '',
};

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    margin: theme.spacing(1),
  },
  withoutLabel: {
    marginTop: theme.spacing(3),
  },
  textField: {
    width: '25ch',
  },
}));

const theme = createMuiTheme({
  overrides: {
    MuiTooltip: {
      tooltip: {
        fontSize: "1em",
        // color: "yellow",
        // backgroundColor: "red",
      }
    }
  }
})

interface State {
  newPassword: string;
  repeatNewPassword: string;
  showNewPassword: boolean;
  showPassword: boolean;
}

function SignupPage(props: {
  saveNewPassword?: any;
  saveUserInfo?: any;
  savePingErrorData?: any;
  intl?: any;
  logout?: any;
}) {
  const { intl } = props;
  const [loading, setLoading] = useState(false);
  const [isRequested, setIsRequested] = useState(false);

  const history = useHistory();

  const classes = useStyles();
  const [values, setValues] = React.useState<State>({
    newPassword: '',
    repeatNewPassword: '',
    showNewPassword: false,
    showPassword: false,
  });

  const notifyError = (error: string) => {
    toast.error(intl.formatMessage({ id: error ?? 'COMMON_COMPONENT.TOAST.DEFAULT_ERROR' }), {
      position: 'top-right',
      autoClose: 2500,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: false,
      progress: undefined,
    });
  };

  const notifySuccess = (message: string) => {
    toast.success(intl.formatMessage({ id: message ?? 'COMMON_COMPONENT.TOAST.DEFAULT_SUCCESS' }), {
      position: 'top-right',
      autoClose: 2500,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: false,
      progress: undefined,
    });
  }

  const location = window.location;
  const { pathname } = location;
  const { search } = location;

  let callbackUrl = new URLSearchParams(search).get('callbackUrl');
  callbackUrl = callbackUrl ? callbackUrl : pathname;
  const userInfo = useSelector(({ auth }: { auth: RootStateOrAny }) => auth);
  const ChangePasswordSchema = Yup.object().shape({
    newPassword: Yup.string()
      .min(8, intl.formatMessage({
        id: "CHANGE_PASSWORD.PASS_LENGTH_RANGE",
      }))
      .max(32, intl.formatMessage({
        id: "CHANGE_PASSWORD.PASS_LENGTH_RANGE",
      }))
      .matches(/^\S*$/, intl.formatMessage({
        id: "CHANGE_PASSWORD.NOT_ALLOW_SPACE",
      }))
      .matches(/(?!^\d+$)^.+$/, intl.formatMessage({
        id: "CHANGE_PASSWORD.PASS_NEED_2OF3",
      }))
      .matches(/(?!^[A-Za-z]+$)^.+$/, intl.formatMessage({
        id: "CHANGE_PASSWORD.PASS_NEED_2OF3",
      }))
      .matches(/(?!^[!@#$%&*/=?^+'/_.,:;-\\]+$)^.+$/, intl.formatMessage({
        id: "CHANGE_PASSWORD.PASS_NEED_2OF3",
      }))
      .required(
        intl.formatMessage({
          id: 'AUTH.VALIDATION.NEWPW_REQUIRED_FIELD',
        }),
      ),
    repeatNewPassword: Yup.string()
      .required(
        intl.formatMessage({
          id: 'AUTH.VALIDATION.CFPW_REQUIRED_FIELD',
        }),
      )
      .when('newPassword', {
        is: val => (!!(val && val.length > 0)),
        then: Yup.string().oneOf(
          [Yup.ref('newPassword')],
          intl.formatMessage({ id: 'CHANGE_PASSWORD.RETYPE_NOT_MATCH' }),
        ),
      }),
  });

  const handleClickShowPassword = (prop: keyof State, show: boolean) => {
    setValues({ ...values, [prop]: !show });
  };

  const handleChange = (prop: keyof State) => (event: React.ChangeEvent<HTMLInputElement>) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
  };

  const cancleClick = () => {
    history.push('' + callbackUrl);
  };
  
  const changePassword = (
    { newPassword, privateKey }: { newPassword: string; privateKey: string },
    { setStatus, setSubmitting }: { setStatus: any; setSubmitting: any },
  ) => {

    const keyPair = GenerateKeyPair(null);
    const encryptedPrivateKey = SymmetricEncrypt(keyPair.privateKey, newPassword);

    const params = QueryString.parse(search.substring(1));
    const otp = params?.otp?.toString();
    const otpId = params?.otpId?.toString();

    if (otp && otpId) {
      const data = {
        otpId,
        otp,
        publicKey: keyPair.publicKey,
        encryptedPrivateKey,
      }
      Signup(data).then((res: any) => {
        if (!res.error) {
          notifySuccess('ADMIN.SIGNUP.SUCCESS_MSG')
          setLoading(true)
          if (userInfo) {
            props.logout();
          }
          setTimeout(() => {
            history.push('/auth/login/identifier');
          }, 2000)
        } else {
          notifyError(res.error);
        }
      }).catch(err => {
        notifyError('SIGNUP.SIGNUP_FAILED');
      })
    }
  };

  const formik = useFormik({
    initialValues,
    validationSchema: ChangePasswordSchema,
    onSubmit: (values, ob) => {
      return changePassword({ ...values, privateKey: userInfo.privateKey }, ob);
    },
  });
  return (
    <>
      <ToastContainer />
      <div className="login-form login-signin" style={{ display: 'block', zIndex: 1 }}>
        <div className="text-center mb-10 mb-lg-10">
          <h3 className="font-size-h1">
            <FormattedMessage id="AUTH.SIGNUP.TITLE_1" />
          </h3>
        </div>
        <div className="text-center mb-10 mb-lg-10">
          <h3 className="font-size-h1">
            <FormattedMessage id="AUTH.SIGNUP.TITLE_2" />
          </h3>
        </div>

        <form
          id="kt_login_signin_form"
          className="form fv-plugins-bootstrap fv-plugins-framework animated animate__animated animate__backInUp"
          onSubmit={formik.handleSubmit}>
          {/* begin: Alert */}
          {formik.status && (
            <div className="mb-10 ml-3 alert alert-custom alert-light-danger alert-dismissible">
              <div className="alert-text font-weight-bold">{formik.status}</div>
            </div>
          )}
          {/* end: Alert */}

          {/* begin: Password */}
          <div className="form-group fv-plugins-icon-container">
            <FormControl
              className={clsx(classes.margin, classes.textField) + ' form-control w-100'}
              variant="outlined">
              <InputLabel
                htmlFor="outlined-adornment-password">{intl.formatMessage({ id: 'AUTH.INPUT.PASSWORD' })}</InputLabel>
              <OutlinedInput
                id="outlined-adornment-password"
                type={values.showNewPassword ? 'text' : 'password'}
                endAdornment={
                  <InputAdornment position="end">
                    <MuiThemeProvider theme={theme}>
                      {
                        values.showNewPassword ? (
                          <Tooltip title={intl.formatMessage({ id: 'AUTH.BUTTON.HIDEPASSWORD' })} placement="right-end"
                            tabIndex={-1}>
                            <IconButton
                              aria-label="toggle password visibility"
                              onClick={e =>
                                handleClickShowPassword('showNewPassword', values.showNewPassword)
                              }
                              onMouseDown={handleMouseDownPassword}
                              edge="end">
                              <Visibility />
                            </IconButton>
                          </Tooltip>
                        ) : (
                          <Tooltip title={intl.formatMessage({ id: 'AUTH.BUTTON.SHOWPASSWORD' })} placement="right-end"
                            tabIndex={-1}>
                            <IconButton
                              aria-label="toggle password visibility"
                              onClick={e =>
                                handleClickShowPassword('showNewPassword', values.showNewPassword)
                              }
                              onMouseDown={handleMouseDownPassword}
                              edge="end">
                              <VisibilityOff />
                            </IconButton>
                          </Tooltip>
                        )
                      }

                    </MuiThemeProvider>
                  </InputAdornment>
                }
                labelWidth={70}
                {...formik.getFieldProps('newPassword')}
              />
            </FormControl>
            {formik.touched.newPassword && formik.errors.newPassword ? (
              <div className="fv-plugins-message-container mt-5 ml-3">
                <div className="fv-help-block">{formik.errors.newPassword}</div>
              </div>
            ) : null}
          </div>
          <div className="form-group fv-plugins-icon-container">
            <FormControl
              className={clsx(classes.margin, classes.textField) + ' form-control w-100'}
              variant="outlined">
              <InputLabel
                htmlFor="outlined-adornment-password">{intl.formatMessage({ id: 'AUTH.INPUT.CONFIRM_PASSWORD' })}</InputLabel>
              <OutlinedInput
                id="outlined-adornment-password-2"
                type={values.showPassword ? 'text' : 'password'}
                // value={values.repeatNewPassword}
                // onChange={handleChange('repeatNewPassword')}
                // name="repeatNewPassword"
                endAdornment={
                  <InputAdornment position="end">
                    <MuiThemeProvider theme={theme}>
                      {values.showPassword ? (
                        <Tooltip title={intl.formatMessage({ id: 'AUTH.BUTTON.HIDEPASSWORD' })} placement="right-end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={e => handleClickShowPassword('showPassword', values.showPassword)}
                            onMouseDown={handleMouseDownPassword}
                            edge="end">
                            {values.showPassword ? <Visibility /> : <VisibilityOff />}
                          </IconButton>
                        </Tooltip>
                      ) : (
                        <Tooltip title={intl.formatMessage({ id: 'AUTH.BUTTON.SHOWPASSWORD' })} placement="right-end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={e => handleClickShowPassword('showPassword', values.showPassword)}
                            onMouseDown={handleMouseDownPassword}
                            edge="end">
                            {values.showPassword ? <Visibility /> : <VisibilityOff />}
                          </IconButton>
                        </Tooltip>
                      )}

                    </MuiThemeProvider>
                  </InputAdornment>
                }
                labelWidth={70}
                {...formik.getFieldProps('repeatNewPassword')}
              />
            </FormControl>
            {formik.touched.repeatNewPassword && formik.errors.repeatNewPassword ? (
              <div className="fv-plugins-message-container mt-6 ml-3">
                <div className="fv-help-block">{formik.errors.repeatNewPassword}</div>
              </div>
            ) : null}
          </div>
          {/* end: Confirm Password */}

          <div className="form-group d-flex flex-wrap flex-center">
            <button type="submit" className="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4">
              <span>{intl.formatMessage({ id: 'COMMON.BTN_SIGNUP' })}</span>
              {loading && <span className="ml-3 spinner spinner-white" />}
            </button>
          </div>
        </form>
      </div>
    </>
  );
}

export default injectIntl(connect(null, auth.actions)(SignupPage));
