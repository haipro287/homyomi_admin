import { useMemo, useState } from "react";
import { useSelector } from "react-redux";
import EntityCrudPage from "../../../common-library/common-components/entity-crud-page"
import { ModifyForm, ModifyInputGroup } from "../../../common-library/common-types/common-type";
import { GetById, Update } from "../../admin/admin.service";
import SaveOutlinedIcon from '@material-ui/icons/SaveOutlined';
import { Spinner } from "react-bootstrap";
import { HomePageURL, iconStyle } from '../../../common-library/common-consts/const';
import { useIntl } from "react-intl";

export const UserProfile = () => {
    const userInfo = useSelector((state: any) => state.auth);
    const intl = useIntl();

    const [loading, setLoading] = useState<boolean>(false);

    const actions: any = useMemo(() => ({
        type: 'inside',
        data: {
            save: {
                role: 'submit',
                type: 'submit',
                linkto: 'undefined',
                className: 'btn btn-primary mr-8 fixed-btn-width',
                label: 'SAVE_BTN_LABEL',
                icon: loading ? (<Spinner style={iconStyle} animation="border" variant="light" size="sm" />) :
                    (<SaveOutlinedIcon style={iconStyle} />)
            },
        }
    }), [loading]);

    const group1 = useMemo((): ModifyInputGroup => ({
        _subTitle: ' ',
        _className: 'col-6 pr-xl-15 pr-md-10 pr-5',
        mail: {
            _type: 'string',
            label: 'ADMIN.MY_PROFILE.EMAIL',
            disabled: true,
            required: true,
        },
        role: {
            _type: 'string',
            label: 'ADMIN.MY_PROFILE.ROLE',
            disabled: true,
            required: true,
            formatter: (input: any) => {
                return input ? intl.formatMessage({ id: `ADMIN.MODIFY.TYPE_OPTION.${input?.toString()?.toUpperCase()}` }) : '';
            }
        },
        name: {
            _type: 'string',
            required: true,
            label: 'ADMIN.MY_PROFILE.NAME',
        },
        jobTitle: {
            required: true,
            _type: 'string',
            label: 'ADMIN.MY_PROFILE.JOB_TITLE',
        },
    }), []);

    const updateForm = useMemo((): ModifyForm => ({
        _header: 'MY_ACCOUNT.UPDATE_TITLE',
        panel1: {
            _title: '',
            group1: group1,
        },
    }), [group1]);

    return <EntityCrudPage
        onModify={Update}
        moduleName={"MY_ACCOUNT.TITLE"}
        code={userInfo.id}
        get={GetById}
        formModel={updateForm}
        actions={actions}
        homePageUrl={HomePageURL.user}
        submitDirect={false}
        submitSuccessMsg={"MY_ACCOUNT.CHANGE_INFO_SUCCESS"}
    />
}