import React, { useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { connect } from 'react-redux';
import { FormattedMessage, injectIntl } from 'react-intl';
import { IconButton, InputAdornment, InputLabel, OutlinedInput, TextField } from '@material-ui/core';
import * as auth from '../_redux/auth-redux';
import ErrorIcon from '@material-ui/icons/Error';
import { Login } from '../_redux/auth.service';
import { values } from "lodash";
import { SymmetricDecrypt } from "../service/auth-cryptography";
import { CERTIFICATE_EXP } from "../../../common-library/common-consts/enviroment";
import { Visibility, VisibilityOff } from "@material-ui/icons";
import firebase from "firebase/compat";

/*
  INTL (i18n) docs:
  https://github.com/formatjs/react-intl/blob/master/docs/Components.md#formattedmessage
*/

/*
  Formik+YUP:
  https://jaredpalmer.com/formik/docs/tutorial#getfieldprops
*/

const initialValues = {
  username: '',
  password: '',
};

const LoginUsername = (props: { saveUserInfo?: any; intl?: any; location?: any; savePingErrorData?: any; }) => {
  const { intl } = props;
  const [loading, setLoading] = useState(false);
  const history = useHistory();
  const { search } = window.location;
  let callbackUrl = new URLSearchParams(search).get('callbackUrl');

  const [values, setValues] = useState<any>({
    amount: '',
    password: '',
    showPassword: false,
  });

  const LoginSchema = Yup.object().shape({
    username: Yup.string()
      .matches(/^\S*$/, intl.formatMessage({
        id: 'ERROR.SPACE_NOT_ALLOWED',
      }))
      .required(
        intl.formatMessage({
          id: 'AUTH.VALIDATION.REQUIRED_FIELD',
        }),
      ),
    password: Yup.string()
      .required(
        intl.formatMessage({
          id: 'AUTH.VALIDATION.REQUIRED_FIELD',
        }),
      ),
  });

  useEffect(() => {
    return () => {
      setLoading(false);
    };
  }, []);

  const submitUsername =
    ({ username, password }: { username: string, password: string }, {
      setSubmitting,
      setStatus
    }: { setSubmitting: any, setStatus: any }) => {
      setLoading(true);
      setTimeout(() => {
        Login(username, password)
          .then(async response => {
            const accessToken = (response.data.accessToken) ?? "";
            const refreshToken = (response.data.refreshToken) ?? "";
            const firebaseAccessToken = (response.data.firebaseAccessToken) ?? "";
            localStorage.setItem('userInfo', JSON.stringify({
              username,
              accessToken,
              refreshToken,
              firebaseAccessToken
            }));
            props.saveUserInfo({
              ...response.data,
              username: username,
            });

            const firebaseConfig = {
              apiKey: "AIzaSyDT3ct7yQ5kosW2_Lto2hD9AkBJ3e-NVt0",
              authDomain: "honyomi-45907.firebaseapp.com",
              projectId: "honyomi-45907",
              storageBucket: "honyomi-45907.appspot.com",
              messagingSenderId: "137308750212",
              appId: "1:137308750212:web:69651cb2edefd1c2465dd1",
              measurementId: "G-X5E2DDNBJ4"
            };

            const app = firebase.initializeApp(firebaseConfig);
            const auth = await firebase.auth(app).signInWithCustomToken(firebaseAccessToken).then((userInfo) => {
              console.log(userInfo)
            }).catch(err => {
              setLoading(false);
              setSubmitting(false);
              setStatus(intl.formatMessage({ id: 'AUTH.VALIDATION.FIREBASE_LOGIN_FAILED' }));
            })

            history.push(`${callbackUrl}`)
          })
          .catch(err => {
            setLoading(false);
            setSubmitting(false);
            setStatus(intl.formatMessage({ id: 'AUTH.VALIDATION.INVALID_USERNAME' }),
            );
          });
      }, 200);
    };

  const formik = useFormik({
    initialValues,
    validationSchema: LoginSchema,
    onSubmit: submitUsername,
  });
  const errorMessage = new URLSearchParams(search).get('errorMessage');

  useEffect(() => {
    if (errorMessage && errorMessage != 'null' && errorMessage != 'undefined') {
      formik.setStatus(intl.formatMessage({ id: errorMessage }));
    }
  }, [errorMessage]);

  const handleSubmit = (e: { preventDefault: () => void }) => {
    e.preventDefault();
  };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
  };

  return (
    <div
      className="login-form login-signin p-5"
      id="kt_login_signin_form"
      style={{ zIndex: 1 }}
    >
      {/* begin::Head */}
      <div className="text-center mb-10 mb-lg-10">
        <h3 className="font-size-h1">
          <FormattedMessage id="AUTH.LOGIN.TITLE"/>
        </h3>
        {/* <p className="text-muted font-weight-bold">
          <FormattedMessage id="AUTH.LOGIN.INPUT_NAME.PLACE_HOLDER" />
        </p> */}
      </div>
      {/* end::Head */}

      {/*begin::Form*/}
      <form
        onSubmit={formik.handleSubmit}
        className="form fv-plugins-bootstrap fv-plugins-framework">
        <div className="form-group fv-plugins-icon-container">
          <TextField
            id="outlined-basic"
            autoFocus
            className={`form-control form-control-solid h-auto`}
            label={(<FormattedMessage id="AUTH.LOGIN.USER_NAME"/>)}
            variant="outlined"
            {...formik.getFieldProps('username')}
          />
          {!formik.status && formik.touched.username && formik.errors.username ? (
            <div className="fv-plugins-message-container mt-4">
              <div className="fv-help-block">{formik.errors.username}</div>
            </div>
          ) : null}
          {formik.status && <div className="mt-4 text-danger font-weight-bold"><ErrorIcon/> {formik.status}</div>}
        </div>
        <div className="form-group fv-plugins-icon-container">
          <TextField
            id="outlined-adornment-password"
            className="form-control form-control-solid h-auto"
            style={{ paddingTop: 0, paddingBottom: 0 }}
            label={(<FormattedMessage id="AUTH.LOGIN.PASSWORD"/>)}
            type={values.showPassword ? 'text' : 'password'}
            variant="outlined"
            {...formik.getFieldProps('password')}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                    edge="end">
                    {values.showPassword ? <Visibility/> : <VisibilityOff/>}
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
          {!formik.status && formik.touched.password && formik.errors.password ? (
            <div className="fv-plugins-message-container mt-4">
              <div className="fv-help-block">{formik.errors.password}</div>
            </div>
          ) : null}
        </div>
        <div className="form-group d-flex flex-wrap justify-content-between align-items-center">
          <Link tabIndex={-1}
                to="/auth/forgot-password"
                className="text-dark-50 text-hover-primary my-3 mr-2"
                id="kt_login_forgot">
            <p className="text-muted font-weight-bold"/>
          </Link>
          <button
            id="kt_login_signin_submit"
            type="submit"
            disabled={formik.isSubmitting}
            style={{ zIndex: 1 }}
            className={`btn btn-primary font-weight-bold px-9 py-4 my-3`}>
            <span><FormattedMessage id="AUTH.LOGIN.NEXT_BTN"/></span>
            {loading && <span className="ml-3 spinner spinner-white"/>}
          </button>
          <Link tabIndex={-1}
                to="/auth/forgot-password"
                className="text-dark-50 text-hover-primary my-3 mr-2"
                id="kt_login_forgot">
            <p className="text-muted font-weight-bold"/>
          </Link>
        </div>
      </form>
      {/*end::Form*/}
    </div>
  );
};

export default injectIntl(connect(null, auth.actions)(LoginUsername));
