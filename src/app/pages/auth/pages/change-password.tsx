import React, { useCallback, useEffect, useState } from 'react';
import { useFormik } from 'formik';
import { connect, RootStateOrAny, useSelector } from 'react-redux';
import * as Yup from 'yup';
import { useHistory } from 'react-router-dom';
import { FormattedMessage, injectIntl } from 'react-intl';
import * as auth from '../_redux/auth-redux';
import { Ping, SetPassword } from '../_redux/auth.service';
import { GenerateKeyPair, SignMessage, SymmetricDecrypt, SymmetricEncrypt } from '../service/auth-cryptography';
import { CERTIFICATE_EXP } from '../../../common-library/common-consts/enviroment';
import clsx from 'clsx';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {
  createMuiTheme,
  FormControl,
  IconButton,
  InputAdornment,
  InputLabel,
  makeStyles,
  MuiThemeProvider,
  OutlinedInput,
  Tooltip
} from "@material-ui/core";
import _ from 'lodash';

const initialValues = {
  oldPassword: '',
  newPassword: '',
  repeatNewPassword: '',
};

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    margin: theme.spacing(1),
  },
  withoutLabel: {
    marginTop: theme.spacing(3),
  },
  textField: {
    width: '25ch',
  },
}));

const theme = createMuiTheme({
  overrides: {
    MuiTooltip: {
      tooltip: {
        fontSize: "1em",
        // color: "yellow",
        // backgroundColor: "red",
      }
    }
  }
})

interface State {
  oldPassword: string;
  showOldPassword: boolean;
  newPassword: string;
  repeatNewPassword: string;
  showNewPassword: boolean;
  showPassword: boolean;
}

function ChangePasswordPage(props: {
  saveNewPassword?: any;
  saveUserInfo?: any;
  savePingErrorData?: any;
  intl?: any;
}) {
  const { intl } = props;
  const [loading, setLoading] = useState(false);
  const [isRequested, setIsRequested] = useState(false);
  const [countTime, setCountTime] = useState(0);

  const history = useHistory();

  const classes = useStyles();
  const [values, setValues] = React.useState<State>({
    oldPassword: '',
    showOldPassword: false,
    newPassword: '',
    repeatNewPassword: '',
    showNewPassword: false,
    showPassword: false,
  });

  useEffect(() => {
    if (countTime > 0) {
      setTimeout(() => {
        setCountTime(countTime - 1);
      }, 1000);
    }
  }, [countTime]);

  const notifyError = (error: string) => {
    toast.error(intl.formatMessage({ id: error ?? 'COMMON_COMPONENT.TOAST.DEFAULT_ERROR' }), {
      position: 'top-right',
      autoClose: 2500,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: false,
      progress: undefined,
    });
  };

  const notifySuccess = (message: string) => {
    toast.success(intl.formatMessage({ id: message ?? 'COMMON_COMPONENT.TOAST.DEFAULT_SUCCESS' }), {
      position: 'top-right',
      autoClose: 2500,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: false,
      progress: undefined,
    });
  }

  const location = window.location;
  const { pathname } = location;
  const { search } = location;
  let callbackUrl = new URLSearchParams(search).get('callbackUrl');
  callbackUrl = callbackUrl ? callbackUrl : pathname;
  const userInfo = useSelector(({ auth }: { auth: RootStateOrAny }) => auth);
  const ChangePasswordSchema = Yup.object().shape({
    oldPassword: Yup.string()
      .required(
        intl.formatMessage({
          id: 'AUTH.VALIDATION.OLDPW_REQUIRED_FIELD',
        }),
      ),
    newPassword: Yup.string()
      .min(8, intl.formatMessage({
        id: "CHANGE_PASSWORD.PASS_LENGTH_RANGE",
      }))
      .max(32, intl.formatMessage({
        id: "CHANGE_PASSWORD.PASS_LENGTH_RANGE",
      }))
      // .matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/, "CHANGE_PASSWORD.PASS_NEED_2OF3") 
      // .matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/, "CHANGE_PASSWORD.PASS_NEED_2OF3") 
      // .matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d@$!%*#?&]{8,}$/, "CHANGE_PASSWORD.PASS_NEED_2OF3") 
      // .matches(/^[a-zA-Z]+$/, "CHANGE_PASSWORD.PASS_NEED_2OF3") 
      // .matches(/^[0-9]+$/, "CHANGE_PASSWORD.PASS_NEED_2OF3") 
      // .matches(/^[!@#$%&/=?_.,:;-\\]+$/, "CHANGE_PASSWORD.PASS_NEED_2OF3") 
      .matches(/^\S*$/, intl.formatMessage({
        id: "CHANGE_PASSWORD.NOT_ALLOW_SPACE",
      }))
      .matches(/(?!^\d+$)^.+$/, intl.formatMessage({
        id: "CHANGE_PASSWORD.PASS_NEED_2OF3",
      }))
      .matches(/(?!^[A-Za-z]+$)^.+$/, intl.formatMessage({
        id: "CHANGE_PASSWORD.PASS_NEED_2OF3",
      }))
      .matches(/(?!^[!@#$%&*/=?^+'/_.,:;-\\]+$)^.+$/, intl.formatMessage({
        id: "CHANGE_PASSWORD.PASS_NEED_2OF3",
      }))
      .required(
        intl.formatMessage({
          id: 'AUTH.VALIDATION.NEWPW_REQUIRED_FIELD',
        }),
      ),
    repeatNewPassword: Yup.string()
      .required(
        intl.formatMessage({
          id: 'AUTH.VALIDATION.CFPW_REQUIRED_FIELD',
        }),
      )
      .when('newPassword', {
        is: val => (!!(val && val.length > 0)),
        then: Yup.string().oneOf(
          [Yup.ref('newPassword')],
          intl.formatMessage({ id: 'CHANGE_PASSWORD.RETYPE_NOT_MATCH' }),
        ),
      }),
  });

  const handleClickShowPassword = (prop: keyof State, show: boolean) => {
    setValues({ ...values, [prop]: !show });
  };

  const handleChange = (prop: keyof State) => (event: React.ChangeEvent<HTMLInputElement>) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
  };

  const cancleClick = () => {
    history.push('' + callbackUrl);
  };
  const GenerateCertificate = (
    certificateInfo: { id: string; timestamp: any; exp: number },
    privateKey: string,
  ) => {
    const keyPair = GenerateKeyPair(privateKey);
    const signature = SignMessage(privateKey, certificateInfo);
    return { signature, certificateInfo, publicKey: keyPair.publicKey };
  };
  const changePassword = (
    { oldPassword, newPassword, privateKey }: { oldPassword: string; newPassword: string; privateKey: string },
    { setStatus, setSubmitting }: { setStatus: any; setSubmitting: any },
  ) => {

    const encryptedPrivateKey = userInfo.encryptedPrivateKey
    const oldPrivateKey = SymmetricDecrypt(encryptedPrivateKey, oldPassword) ?? "";
    // const oldCertificate = GenerateCertificate(
    //   {
    //     id: userInfo.id,
    //     timestamp: new Date(),
    //     exp: CERTIFICATE_EXP,
    //   },
    //   oldPrivateKey,
    // );

    // Ping(oldCertificate, userInfo).then((data: any) => {
    // if (data.id) {
    if (oldPrivateKey === userInfo._privateKey) {
      const keyPair = GenerateKeyPair(null);
      const encryptedPrivateKey = SymmetricEncrypt(keyPair.privateKey, newPassword);
      const data = {
        publicKey: keyPair.publicKey,
        encryptedPrivateKey,
      };
      // SetTempPassword(data).then(res => {
      SetPassword(data).then(res => {
        //TODO:Save new credential
        if (res.data?.publicKey) {
          const certificate = GenerateCertificate(
            {
              id: userInfo.id,
              timestamp: Math.floor(new Date().getTime()),
              exp: CERTIFICATE_EXP,
            },
            keyPair.privateKey,
          );

          props.saveNewPassword({
            publicKey: keyPair.publicKey,
            _certificate: certificate,
            _privateKey: keyPair.privateKey,
          });

          props.saveUserInfo({
            ...res.data,
            _certificate: certificate,
            _privateKey: keyPair.privateKey,
            _preLoggedIn: false,
          });

          notifySuccess('CHANGE_PASSWORD.CHANGE_SUCCESS');
          setCountTime(2);
          const location = window.location;
          const { pathname } = location;
          const { search } = location;
          const temp = new URLSearchParams(search).get('callbackUrl');
          let callbackUrl = temp ? temp : pathname;
          setTimeout(() => {
            history.push(callbackUrl);
          }, 2000);
        }
        else {
          notifyError('CHANGE_PASSWORD.CHANGE_FAILED');
          // setStatus(intl.formatMessage({ id: 'CHANGE_PASSWORD.WRONG_PASSWORD' }));;
          // formik.errors.oldPassword = intl.formatMessage({ id: 'CHANGE_PASSWORD.CHANGE_FAILED' })
        }
      }).catch(err => {
        notifyError('CHANGE_PASSWORD.CHANGE_FAILED');
        // formik.errors.oldPassword = intl.formatMessage({ id: 'CHANGE_PASSWORD.CHANGE_FAILED' })
        // setStatus(intl.formatMessage({ id: 'CHANGE_PASSWORD.WRONG_PASSWORD' }));;
      });
    } else {
      // formik.errors.oldPassword = intl.formatMessage({ id: 'CHANGE_PASSWORD.CHANGE_FAILED' })
      // setStatus(intl.formatMessage({ id: 'CHANGE_PASSWORD.WRONG_PASSWORD' }));;
      notifyError('CHANGE_PASSWORD.WRONG_PASSWORD');
    }
    // }).catch(err => {
    // setStatus(intl.formatMessage({id: 'CHANGE_PASSWORD.CHANGE_FAILED'}));;
    // notifyError('CHANGE_PASSWORD.WRONG_PASSWORD');
    // });
  };

  const formik = useFormik({
    initialValues,
    validationSchema: ChangePasswordSchema,
    onSubmit: (values, ob) => {
      return changePassword({ ...values, privateKey: userInfo.privateKey }, ob);
    },
  });
  return (
    <>
      <ToastContainer />
      <div className="login-form login-signin" style={{ display: 'block', zIndex: 1 }}>
        <div className="text-center mb-10 mb-lg-10">
          <h3 className="font-size-h1">
            <FormattedMessage id="AUTH.CHANGEPASSWORD.TITLE" />
          </h3>
          <p className="text-muted font-weight-bold">{intl.formatMessage({ id: 'AUTH.CHANGEPASSWORD.SUB_TITLE' })}</p>
        </div>

        <form
          id="kt_login_signin_form"
          className="form fv-plugins-bootstrap fv-plugins-framework animated animate__animated animate__backInUp"
          onSubmit={formik.handleSubmit}>
          {/* begin: Alert */}
          {formik.status && (
            <div className="mb-10 ml-3 alert alert-custom alert-light-danger alert-dismissible">
              <div className="alert-text font-weight-bold">{formik.status}</div>
            </div>
          )}
          {/* end: Alert */}

          {/* begin: Password */}
          <div className="form-group fv-plugins-icon-container">
            <FormControl
              className={clsx(classes.margin, classes.textField) + ' form-control w-100'}
              variant="outlined">
              <InputLabel
                htmlFor="outlined-adornment-oldpassword">{intl.formatMessage({ id: 'AUTH.FORGOT_PASSWORD.INPUT.OLD_PASSWORD' })}</InputLabel>
              <OutlinedInput
                id="outlined-old-adornment-oldpassword"
                type={values.showOldPassword ? 'text' : 'password'}
                endAdornment={
                  <InputAdornment position="end">
                    <MuiThemeProvider theme={theme}>
                      {
                        values.showOldPassword ? (
                          <Tooltip title={intl.formatMessage({ id: 'AUTH.BUTTON.HIDEPASSWORD' })} placement="right-end"
                            tabIndex={-1}>
                            <IconButton
                              aria-label="toggle password visibility"
                              onClick={e =>
                                handleClickShowPassword('showOldPassword', values.showOldPassword)
                              }
                              onMouseDown={handleMouseDownPassword}
                              edge="end">
                              <Visibility />
                            </IconButton>
                          </Tooltip>
                        ) : (
                          <Tooltip title={intl.formatMessage({ id: 'AUTH.BUTTON.SHOWPASSWORD' })} placement="right-end"
                            tabIndex={-1}>
                            <IconButton
                              aria-label="toggle password visibility"
                              onClick={e =>
                                handleClickShowPassword('showOldPassword', values.showOldPassword)
                              }
                              onMouseDown={handleMouseDownPassword}
                              edge="end">
                              <VisibilityOff />
                            </IconButton>
                          </Tooltip>
                        )
                      }

                    </MuiThemeProvider>
                  </InputAdornment>
                }
                labelWidth={70}
                {...formik.getFieldProps('oldPassword')}
              />
            </FormControl>
            {formik.touched.oldPassword && formik.errors.oldPassword ? (
              <div className="fv-plugins-message-container mt-5 ml-3">
                <div className="fv-help-block">{formik.errors.oldPassword}</div>
              </div>
            ) : null}
          </div>
          <div className="form-group fv-plugins-icon-container">
            <FormControl
              className={clsx(classes.margin, classes.textField) + ' form-control w-100'}
              variant="outlined">
              <InputLabel
                htmlFor="outlined-adornment-password">{intl.formatMessage({ id: 'AUTH.FORGOT_PASSWORD.INPUT.PASSWORD' })}</InputLabel>
              <OutlinedInput
                id="outlined-adornment-password"
                type={values.showNewPassword ? 'text' : 'password'}
                endAdornment={
                  <InputAdornment position="end">
                    <MuiThemeProvider theme={theme}>
                      {
                        values.showNewPassword ? (
                          <Tooltip title={intl.formatMessage({ id: 'AUTH.BUTTON.HIDEPASSWORD' })} placement="right-end"
                            tabIndex={-1}>
                            <IconButton
                              aria-label="toggle password visibility"
                              onClick={e =>
                                handleClickShowPassword('showNewPassword', values.showNewPassword)
                              }
                              onMouseDown={handleMouseDownPassword}
                              edge="end">
                              <Visibility />
                            </IconButton>
                          </Tooltip>
                        ) : (
                          <Tooltip title={intl.formatMessage({ id: 'AUTH.BUTTON.SHOWPASSWORD' })} placement="right-end"
                            tabIndex={-1}>
                            <IconButton
                              aria-label="toggle password visibility"
                              onClick={e =>
                                handleClickShowPassword('showNewPassword', values.showNewPassword)
                              }
                              onMouseDown={handleMouseDownPassword}
                              edge="end">
                              <VisibilityOff />
                            </IconButton>
                          </Tooltip>
                        )
                      }

                    </MuiThemeProvider>
                  </InputAdornment>
                }
                labelWidth={70}
                {...formik.getFieldProps('newPassword')}
              />
            </FormControl>
            {formik.touched.newPassword && formik.errors.newPassword ? (
              <div className="fv-plugins-message-container mt-5 ml-3">
                <div className="fv-help-block">{formik.errors.newPassword}</div>
              </div>
            ) : null}
          </div>
          <div className="form-group fv-plugins-icon-container">
            <FormControl
              className={clsx(classes.margin, classes.textField) + ' form-control w-100'}
              variant="outlined">
              <InputLabel
                htmlFor="outlined-adornment-password">{intl.formatMessage({ id: 'AUTH.FORGOT_PASSWORD.INPUT.CONFIRM_PASSWORD' })}</InputLabel>
              <OutlinedInput
                id="outlined-adornment-password-2"
                type={values.showPassword ? 'text' : 'password'}
                // value={values.repeatNewPassword}
                // onChange={handleChange('repeatNewPassword')}
                // name="repeatNewPassword"
                endAdornment={
                  <InputAdornment position="end">
                    <MuiThemeProvider theme={theme}>
                      {values.showPassword ? (
                        <Tooltip title={intl.formatMessage({ id: 'AUTH.BUTTON.HIDEPASSWORD' })} placement="right-end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={e => handleClickShowPassword('showPassword', values.showPassword)}
                            onMouseDown={handleMouseDownPassword}
                            edge="end">
                            {values.showPassword ? <Visibility /> : <VisibilityOff />}
                          </IconButton>
                        </Tooltip>
                      ) : (
                        <Tooltip title={intl.formatMessage({ id: 'AUTH.BUTTON.SHOWPASSWORD' })} placement="right-end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={e => handleClickShowPassword('showPassword', values.showPassword)}
                            onMouseDown={handleMouseDownPassword}
                            edge="end">
                            {values.showPassword ? <Visibility /> : <VisibilityOff />}
                          </IconButton>
                        </Tooltip>
                      )}

                    </MuiThemeProvider>
                  </InputAdornment>
                }
                labelWidth={70}
                {...formik.getFieldProps('repeatNewPassword')}
              />
            </FormControl>
            {formik.touched.repeatNewPassword && formik.errors.repeatNewPassword ? (
              <div className="fv-plugins-message-container mt-6 ml-3">
                <div className="fv-help-block">{formik.errors.repeatNewPassword}</div>
              </div>
            ) : null}
          </div>
          {/* end: Confirm Password */}

          <div className="form-group d-flex flex-wrap flex-center">
            <button type="submit" disabled={loading || countTime > 0}className="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4">
              <span>{intl.formatMessage({ id: 'COMMON.BTN_CHANGE_PW' })}</span>
              {(loading || countTime > 0) && <span className="ml-3 spinner spinner-white" />}
            </button>

            <button
              type="button"
              className="btn btn-outline-primary font-weight-bold px-9 py-4 my-3 mx-4"
              onClick={cancleClick}>
              {intl.formatMessage({ id: 'COMMON.BTN_CANCEL' })}
            </button>
          </div>
        </form>
      </div>
    </>
  );
}

export default injectIntl(connect(null, auth.actions)(ChangePasswordPage));
