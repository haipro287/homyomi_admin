import axios from 'axios';
import { API_BASE_URL } from '../../../common-library/common-consts/enviroment';

const BASE_URL = API_BASE_URL + '/auth';
export const LOGIN_URL = BASE_URL + '/login';
export const PING_URL = BASE_URL + '/ping-admin';
export const IDENTITY_URL = BASE_URL + '/identity/';

export const REQUEST_PASSWORD_URL = BASE_URL + '/password/forgot';
export const CHANGE_PASSWORD_URL = BASE_URL + '/password';
export const RESET_PASSWORD_URL = BASE_URL + '/otp/forgot';
export const SET_TEMP_PASSWORD_URL = BASE_URL + '/temp-password';
export const SIGNUP_URL = BASE_URL + '/otp/admin';

export function Login(username: string, password: string) {
  return axios.post(LOGIN_URL, { username, password });
}

export const Signup = (data: {
  otpId: string,
  otp: string,
  publicKey: string,
  encryptedPrivateKey: string,
}) => {
  return axios.post(SIGNUP_URL, { data });
}

export const Ping = (certificate: {
  signature: string;
  certificateInfo:
  { id: string; exp: number; timestamp: any; };
  publicKey: string;
}, userInfo: any) => {
  // const customAxios = createCustomAxios(userInfo);
  return axios.post(PING_URL, certificate);
};

export function requestPassword(email: string) {
  return axios.post(REQUEST_PASSWORD_URL, { data: { mail: email } });
}

export const ChangePassword = (data: any) => {
  return axios.post(CHANGE_PASSWORD_URL, data);
};

export const SetTempPassword = (data: { publicKey: string; encryptedPrivateKey: string }) => {
  return axios.post(SET_TEMP_PASSWORD_URL, data);
};

export const SetPassword = (data: { publicKey: string; encryptedPrivateKey: string }) => {
  return axios.post(CHANGE_PASSWORD_URL, data);
};

export const ResetChangePassword = (data: {
  otp: string;
  otpId: string;
  publicKey: string;
  encryptedPrivateKey: string
}) => {
  return axios.post(RESET_PASSWORD_URL, { data: data });
};

export function getUserFromIdentity(username: string) {
  return axios.get(IDENTITY_URL + username);
}

export function CheckOtpStatus(
  otp: string,
  otpId: string,
) {
  return axios.post(BASE_URL + '/otp', { data: { otp, otpId } });
}

//
// export function getUserFromIdentity(username) {
//     return axios.get(IDENTITY_URL + username);
// }

export function saveIdentity(
  username: any,
  rs_password: any,
  signature: any,
  en_private_key: any,
  public_key: any,
) {
  return axios.post(IDENTITY_URL, { username, rs_password, signature, en_private_key, public_key });
}
