/**
 * High level router.
 *
 * Note: It's recommended to compose related routes in internal router
 * common-library (e.g: `src/app/modules/auth/pages/AuthPage`, `src/app/BasePage`).
 */

import { Redirect, Route, Switch, useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';
import BasePage from './base-page';
import { Layout } from './layout/components/layout';
import ErrorsPage from './layout/errors/errors-page';
import { AuthPage, Logout } from './pages/auth';
import store from '../redux/store';
import firebase from "firebase/compat";
import { useEffect } from "react";

export function Routes() {
  const userInfo = useSelector(({ auth }: any) => auth);
  const location = useLocation();
  const { pathname } = location;
  const { search } = location;
  const temp = new URLSearchParams(search).get('callbackUrl');
  let callbackUrl = temp ? temp : pathname;
  const isAuthUrls = callbackUrl.indexOf('/logout') > -1 || callbackUrl.indexOf('/auth/') > -1;
  callbackUrl = !isAuthUrls ? callbackUrl : '/';

  // let username = location.pathname === '/auth/login/identifier' ? null : userInfo.username;
  let username = userInfo.username;

  useEffect( () => {
    let disposed = false;

    console.log(userInfo.firebaseAccessToken);
    (async () => {
      const firebaseConfig = {
        apiKey: "AIzaSyDT3ct7yQ5kosW2_Lto2hD9AkBJ3e-NVt0",
        authDomain: "honyomi-45907.firebaseapp.com",
        projectId: "honyomi-45907",
        storageBucket: "honyomi-45907.appspot.com",
        messagingSenderId: "137308750212",
        appId: "1:137308750212:web:69651cb2edefd1c2465dd1",
        measurementId: "G-X5E2DDNBJ4"
      };

      const app = firebase.initializeApp(firebaseConfig);
      const auth = await firebase.auth(app).signInWithCustomToken(userInfo.firebaseAccessToken).then((userInfo) => {
        console.log(userInfo)
      }).catch(err => {
        // setStatus(intl.formatMessage({ id: 'AUTH.VALIDATION.FIREBASE_LOGIN_FAILED' }));
      })
    })()

    return () => {disposed = true}
  }, [])

  const CheckAuth = () => {
    const state: any = store.getState();
    // const username = state.auth.username;
    {
      return (
        !username ?
          <Route>
            <AuthPage/>
            <Redirect
              to={`/auth/login/identifier?callbackUrl=${callbackUrl}`}
            />
          </Route> :
          <Route>
            <Redirect from={'/auth'} to={callbackUrl} key={'r_base'}/>,
            <Layout key={'base'}>
              <BasePage/>
            </Layout>,
          </Route>
      );
    }
  };
  return (
    <Switch>
      <Route path="/error" component={ErrorsPage}/>
      <Route path="/logout" component={Logout}/>
      {CheckAuth()}
    </Switch>
  );
}
