const axios = require('axios');
const qs = require('qs');
const FormData = require('form-data');

const _getActionModule = (_url: any) => {
    const pathname =
        _url[0] === '/' ? String(_url) : String(new URL(_url).pathname);
    return pathname.replace(new RegExp('/', 'g'), '-').substring(1);
};

const _setupAxios = (originAxios: any, auth: any) => {
    originAxios.interceptors.request.use(
        (config: any) => {
            config.paramsSerializer = (params: any) => {
                return qs.stringify(params, {
                    allowDots: true,
                    arrayFormat: 'comma',
                    encode: false,
                });
            };
            config.headers["Authorization"] = `Bearer ${window.localStorage.getItem("accessToken")}`;
            return config;
        },
        (err: any) => Promise.reject(err)
    );

    originAxios.interceptors.response.use(
        (next: any) => {
            if (!next.data.success) return Promise.reject(next.data.reason);
            return Promise.resolve(next.data.data);
        },
        (error: any) => {
            if (!error.response) return Promise.reject(error);
            const errorCode = error.response.data;
            return Promise.reject(error);
        }
    );
};

export const createCustomAxios = (userInfo: { username: string, _privateKey: string, id: string, _certificate: any }) => {
    const _axios = axios.create();
    _setupAxios(_axios, userInfo);
    return _axios;
};

